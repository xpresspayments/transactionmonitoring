module.exports = {
  runtimeCompiler: undefined,
  productionSourceMap: false,
  publicPath: 'xpressmonitor',

  chainWebpack: config => {
    config.module
        .rule('raw')
        .test(/\.txt$/)
        .use('raw-loader')
        .loader('raw-loader')
        .end()
  },

  outputDir: 'xpressmonitor',
  assetsDir: undefined,
  parallel: undefined,
  css: undefined,
  /*devServer: {
    host: "monitor.xpresspayments.com"
  }*/
}
