import axios from 'axios';

const API_URL = 'http://localhost:5000/api/Auth/Login';

class AuthService {
  login(user) {
    return axios
      .post(API_URL, {
        email: btoa(user.email),
        password: btoa(user.password)
      })
      .then(response => {
        if (response.data.responseCode === "00") {

          localStorage.setItem('user', JSON.stringify(response.data));

          return response.data;

        }else{

          return response.data;
        }

        

      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();