import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store'

import BootstrapVue from "bootstrap-vue"
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate);
Vue.use(BootstrapVue);

Vue.config.productionTip = true;

import Default from './Layout/Wrappers/baseLayout.vue';
import Pages from './Layout/Wrappers/pagesLayout.vue';
import Apps from './Layout/Wrappers/appLayout.vue';
import Layouts from './Layout/Wrappers/layoutsExamples.vue';

import PageTitle from './Layout/Components/PageTitle'
Vue.component('PageTitle', PageTitle);

Vue.component('default-layout', Default);
Vue.component('pages-layout', Pages);
Vue.component('apps-layout', Apps);
Vue.component('examples-layout', Layouts);

new Vue({
  router,
  store,
render: h => h(App)
}).$mount('#app');