import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    scrollBehavior() {
        return window.scrollTo({ top: 0, behavior: 'smooth' });
    },
    routes: [

        {
            path: '/',
            meta: {layout: 'examples'},
            name: 'login',
            component: () => import('./examples/user-pages/login.vue'),
        },

        {
            path: '/user-set-pwd',
            meta: {layout: 'examples'},
            name: 'set-password',
            component: () => import('./examples/user-pages/set_password.vue'),
        },

        // Dashboards

        {
            path: '/home',
            name: 'web-analytics',
            component: () => import('./views/dashboards/monitoring.vue'),
        },
        {
            path: '/approved-txn',
            name: 'all-approved',
            component: () => import('./views/dashboards/all-approved.vue'),
        },
        {
            path: '/declined-txn',
            name: 'all-declined',
            component: () => import('./views/dashboards/all-declined.vue'),
        },
        {
            path: '/terminal/health-check',
            name: 'monitoring',
            meta: {layout: 'pages'},
            component: () => import('./views/dashboards/web-analytics.vue'),
        },

        // Layouts

        /*{
            path: '/layouts/light-sidebar',
            name: 'light-sidebar',
            meta: {layout: 'examples'},
            component: () => import('./views/layouts/light-sidebar.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/layouts/dark-sidebar',
            name: 'dark-sidebar',
            meta: {layout: 'examples'},
            component: () => import('./views/layouts/dark-sidebar.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/layouts/gradient-sidebar-1',
            name: 'gradient-sidebar-1',
            meta: {layout: 'examples'},
            component: () => import('./views/layouts/gradient-sidebar-1.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/layouts/gradient-sidebar-2',
            name: 'gradient-sidebar-2',
            meta: {layout: 'examples'},
            component: () => import('./views/layouts/gradient-sidebar-2.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/layouts/primary-sidebar',
            name: 'primary-sidebar',
            meta: {layout: 'examples'},
            component: () => import('./views/layouts/primary-sidebar.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // Applications

        {
            path: '/applications/apps-widgets',
            name: 'apps-widgets',
            component: () => import('./examples/ui-elements/app-boxes.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/applications/chat',
            name: 'chat',
            meta: {layout: 'apps'},
            component: () => import('./views/applications/chat.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/applications/contacts',
            name: 'contacts',
            meta: {layout: 'apps'},
            component: () => import('./views/applications/contacts.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/applications/file-manager',
            name: 'file-manager',
            meta: {layout: 'apps'},
            component: () => import('./views/applications/file-manager.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/applications/mail',
            name: 'mail',
            meta: {layout: 'apps'},
            component: () => import('./views/applications/mail.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/applications/events-calendar',
            name: 'events-calendar',
            meta: {layout: 'apps'},
            component: () => import('./views/applications/events-calendar.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },*/

        // User pages
        {
            path: '/user-access/all',
            name: 'view-users',
            component: () => import('./examples/tables/user-list.vue'),

        },
        //GROUPS
        {
            path: '/merchants/groups',
            name: 'view-groups',
            component: () => import('./examples/tables/xpress-merchant-groups'),

        },
        {
            path: '/bank-access',
            name: 'bank-management',
            component: () => import('./examples/tables/xpress-bank-access.vue'),

        },
        

        /* {
            path: '/user-access/new',
            name: 'new-user',
            component: () => import('./examples/forms/create-user-element.vue'),

        },

       {
            path: '/user-pages/login',
            meta: {layout: 'examples'},
            name: 'login',
            component: () => import('./examples/user-pages/login.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/user-pages/register',
            meta: {layout: 'examples'},
            name: 'register',
            component: () => import('./examples/user-pages/register.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/user-pages/recover-password',
            meta: {layout: 'examples'},
            name: 'recover-password',
            component: () => import('./examples/user-pages/recover-password.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/user-pages/profile',
            name: 'profile',
            meta: {layout: 'apps'},
            component: () => import('./examples/user-pages/profile.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // Other pages

        {
            path: '/other-pages/error-404',
            meta: {layout: 'examples'},
            name: 'error-404',
            component: () => import('./examples/other-pages/error-404.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/other-pages/error-500',
            meta: {layout: 'examples'},
            name: 'error-500',
            component: () => import('./examples/other-pages/error-500.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/other-pages/error-505',
            meta: {layout: 'examples'},
            name: 'error-505',
            component: () => import('./examples/other-pages/error-505.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/other-pages/invoice',
            name: 'invoice',
            component: () => import('./examples/other-pages/invoice.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // Single pages

        {
            path: '/ui-kit-pro-included',
            name: 'ui-kit-pro-included',
            component: () => import('./examples/other-pages/ui-kit-pro.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/maps',
            name: 'maps',
            component: () => import('./examples/maps.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/charts',
            name: 'charts',
            component: () => import('./examples/charts.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // UI Elements

        {
            path: '/elements/colors',
            name: 'colors',
            component: () => import('./examples/ui-elements/colors.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/buttons',
            name: 'buttons',
            component: () => import('./examples/ui-elements/buttons.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/badges',
            name: 'badges',
            component: () => import('./examples/ui-elements/badges.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/avatars',
            name: 'avatars',
            component: () => import('./examples/ui-elements/avatars.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/progress-bars',
            name: 'progress-bars',
            component: () => import('./examples/ui-elements/progress-bars.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/navigation',
            name: 'navigation',
            component: () => import('./examples/ui-elements/navigation.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/alerts',
            name: 'alerts',
            component: () => import('./examples/ui-elements/alerts.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/spinners',
            name: 'spinners',
            component: () => import('./examples/ui-elements/spinners.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/icons',
            name: 'icons',
            component: () => import('./examples/ui-elements/icons.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/timelines',
            name: 'timelines',
            component: () => import('./examples/ui-elements/timelines.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/cards',
            name: 'cards',
            component: () => import('./examples/ui-elements/cards.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/lists',
            name: 'lists',
            component: () => import('./examples/ui-elements/lists.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/modals',
            name: 'modals',
            component: () => import('./examples/ui-elements/modals.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/tooltips',
            name: 'tooltips',
            component: () => import('./examples/ui-elements/tooltips.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/popovers',
            name: 'popovers',
            component: () => import('./examples/ui-elements/popovers.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/carousels',
            name: 'carousels',
            component: () => import('./examples/ui-elements/carousels.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/dropdowns',
            name: 'dropdowns',
            component: () => import('./examples/ui-elements/dropdowns.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/scrollable',
            name: 'scrollable',
            component: () => import('./examples/ui-elements/scrollable.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/tabs',
            name: 'tabs',
            component: () => import('./examples/ui-elements/tabs.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/accordions',
            name: 'accordions',
            component: () => import('./examples/ui-elements/accordions.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/elements/notifications',
            name: 'notifications',
            component: () => import('./examples/ui-elements/notifications.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // Statistics boxes

        {
            path: '/statistics-boxes/horizontal-boxes',
            name: 'horizontal-boxes',
            component: () => import('./examples/statistics-boxes/horizontal-boxes.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/statistics-boxes/blocks',
            name: 'blocks',
            component: () => import('./examples/statistics-boxes/blocks.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/statistics-boxes/simple-blocks',
            name: 'simple-blocks',
            component: () => import('./examples/statistics-boxes/simple-blocks.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/statistics-boxes/chart-boxes',
            name: 'chart-boxes',
            component: () => import('./examples/statistics-boxes/chart-boxes.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/statistics-boxes/card-blocks',
            name: 'card-blocks',
            component: () => import('./examples/statistics-boxes/card-blocks.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // Tables

        {
            path: '/tables/regular-tables',
            name: 'regular-tables',
            component: () => import('./examples/tables/regular-tables.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/tables/sortable-tables',
            name: 'sortable-tables',
            component: () => import('./examples/tables/sortable-tables.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },

        // Forms

        {
            path: '/forms/elements',
            name: 'elements',
            component: () => import('./examples/forms/elements.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/forms/widgets',
            name: 'widgets',
            component: () => import('./examples/forms/widgets.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },
        {
            path: '/forms/fullcalendar',
            name: 'fullcalendar',
            component: () => import('./examples/forms/fullcalendar.vue'),
            //TEMP RESTRICTION OF ACCESS TO TEMPLATE PAGES
            beforeEnter: (to, from, next) => {
                next('/home');
            }
        },*/
    ]
});

router.beforeEach((to, from, next) => {
    NProgress.start()
    NProgress.set(0.1)
    const publicPages = ['/', '/logout', '/user-pages/register', '/user-set-pwd'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');
  
    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
      next('/');
    } else {
      next();
    }
})
router.afterEach(() => {
    setTimeout(() => NProgress.done(), 500)
})

export default router
