import Vue from 'vue'
import Vuex from 'vuex'
import { auth } from './auth.module';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    sidebarCollapsed: true,
    sidebarCollapsedMobile: true
  },
  mutations: {
    SET_SIDEBAR_COLLAPSED(state, value) {
      state.sidebarCollapsed = value;
    },
    SET_SIDEBAR_COLLAPSED_MOBILE(state, value) {
      state.sidebarCollapsedMobile = value;
    }
  },
  modules: {
    auth
  }
});
