﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Enums
{
    public enum Account
    {
        AUDIT = 1,
        FETCH = 2,
        SYSTEMCONFIG = 3,
        UPDREFRESHTOKEN = 4,
        UPDLOGINACTIVITY = 5,
        UPDLOGINATTEMPT = 6,
        UPDLOGOUT = 7,
        ADDUSER = 8,
        CHANGEPASSWORD = 9,
        GETALLUSERS = 10,
        USERSPENDINGAPPROVAL = 11,
        APPROVEUSER = 12,
        DISAPPROVEUSER = 13,
        UPDUSER = 14,
        DEACTIVATEUSER = 15,
        REACTIVATEUSER = 16,
        UNBLOCKUSER = 17,
    }
}
