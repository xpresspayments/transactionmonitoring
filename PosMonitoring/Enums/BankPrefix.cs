﻿using System;
namespace PosMonitoring.Enums
{
    public enum BankPrefix
    {
      GET_ONE_BANK = 1,
      CREATE_NEW_BANK = 2,
      CREATE_NEW_PREFIX = 3,
      GET_ALL_BANKS = 4,
      GET_PREFIXES = 5,
      CREAT_GROUP = 6,
      GET_GROUP = 7,
      GET_ALL_GROUP = 8,
      GET_MERCHANT_GROUP = 9,
      GET_GROUP_MID_NAME = 10,
      ASSIGN_TID_TO_GROUP = 11,
      GET_ALL_TIDS = 12,
      GET_ONE_TERMINAL = 13,
      ASIGN_USER_TO_GROUP = 14,
      ADD_ROLE = 1,
      ASIGN_GRP_TO_MA = 15
    }
}
