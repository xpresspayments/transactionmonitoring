﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PosMonitoring.GenericResponse
{
    public class ApiResponse
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public object Data { get; set; }
    }
}
