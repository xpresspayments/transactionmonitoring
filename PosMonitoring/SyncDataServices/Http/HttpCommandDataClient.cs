﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using PosMonitoring.Dtos;

namespace PosMonitoring.SyncDataServices.Http
{
    public class HttpCommandDataClient : ICommandDataClient
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public HttpCommandDataClient(HttpClient httpClient, IConfiguration confuration)
        {
        
            _httpClient = httpClient;
            _configuration = confuration;
        }

        public async Task SendTransactionToReporting(TransactionReadDto transaction)
        {
            //throw new NotImplementedException();

            var httpContent = new StringContent(
                JsonSerializer.Serialize(transaction),
                Encoding.UTF8,
                "application/json"
                );

            var response = await _httpClient.PostAsync($"{_configuration["ReportingService"]}", httpContent);

            if(response.IsSuccessStatusCode)
            {
                Console.WriteLine("--- Synced POST to reporting was SUCCESSFUL ---");
            }
            else
            {
                Console.WriteLine("--- Synced POST to reporting was NOT SUCCESSFUL ---");
            }
        }
    }
}
