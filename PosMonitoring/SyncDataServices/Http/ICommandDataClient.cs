using System.Threading.Tasks;
using PosMonitoring.Dtos;
namespace PosMonitoring.SyncDataServices.Http
{
    public interface ICommandDataClient
    {
        Task SendTransactionToReporting(TransactionReadDto transaction);
        
    }
}