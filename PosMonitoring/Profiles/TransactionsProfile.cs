﻿using System;
using System.Collections.Generic;
using AutoMapper;
using PosMonitoring.Dtos;
using PosMonitoring.Dtos.Account;
using PosMonitoring.Models;
using PosMonitoring.ViewModels;
using PosMonitoring.Wrappers;

namespace PosMonitoring.Profiles
{
    public class TransactionsProfile : Profile
    {
        public TransactionsProfile()
        {
            //Data source to Target
            CreateMap<Notification, TransactionReadDto>();

            CreateMap<TransactionCreateDto, Notification>().ReverseMap();
            
            CreateMap<TransactionReadDto, TransactionReadDtoVm>().ReverseMap();

            CreateMap<PagedResponse, TransactionReadPagedResponseDto>();

            CreateMap<User, UserViewModel>();

            CreateMap<Banks, BankReadDto>();

            CreateMap<BankCreateDto, Banks>();

            CreateMap<BankPrefixes, BankPrefixCreateDto>();

            CreateMap<BankPrefixes, BankPrefixReadDto>();

            CreateMap<MerchantGroups, MerchantGroupsCreateDto>();

            CreateMap<MerchantGroups, MerchantGroupsReadDto>();

            CreateMap<Terminals, TerminalsCreateDto>();

            CreateMap<Terminals, TerminalsReadDto>();

        }
    }
}
