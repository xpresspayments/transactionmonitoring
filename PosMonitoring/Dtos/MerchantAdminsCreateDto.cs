﻿using System;
namespace PosMonitoring.Dtos
{
    public class MerchantAdminsCreateDto
    {
        public int groupId { get; set; }

        public string maEmail { get; set; }
    }
}
