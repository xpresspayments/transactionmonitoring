﻿using System;
namespace PosMonitoring.Dtos
{
    public class GroupAsignUserCreateDto
    {
        public string UserEmail { get; set; }
        public int GroupId { get; set; }
    }


    public class UserAssignToGroupResponse
    {
        public int UserId { get; set; }
    }


    public class GroupAsignToMerchantAdminDto
    {
        public int groupId { get; set; }
        public string grpMid { get; set; }
        public string maEmail { get; set; }
    }


    public class GroupToMerchantAdminResponse
    {
        public int ma_Id { get; set; }
    }
}
