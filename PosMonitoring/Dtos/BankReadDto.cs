﻿using System;
namespace PosMonitoring.Dtos
{
    public class BankReadDto
    {
        public int Id { get; set; }
        public string BankName { get; set; }
        public string CreatedBy { get; set; }
        public string TerminalIdPrefix { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }


    public class BankCreateResponse
    {
        public dynamic Id { get; set; }
    }
}
