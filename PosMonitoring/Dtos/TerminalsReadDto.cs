﻿using System;
namespace PosMonitoring.Dtos
{
    public class TerminalsReadDto
    {
        public string TrmTerminalId { get; set; }
        public int TrmCreatedBy { get; set; }
    }


    public class TerminalsCreateResponse
    {
        public int Id { get; set; }
    }
}
