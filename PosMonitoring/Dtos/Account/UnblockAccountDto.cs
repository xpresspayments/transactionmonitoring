﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Dtos.Account
{
    public class UnblockAccountDto
    {
        [Required]
        public string Email { get; set; }
    }
}
