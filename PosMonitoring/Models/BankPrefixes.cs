﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PosMonitoring.Models
{
    public class BankPrefixes
    {
        [Key]
        public int Id { get; set; }
        public int BankId { get; set; }
        public string TerminalIdPrefix { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
