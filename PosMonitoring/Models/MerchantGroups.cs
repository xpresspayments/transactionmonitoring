﻿using System;
using System.ComponentModel.DataAnnotations;
namespace PosMonitoring.Models
{
    public class MerchantGroups
    {
        [Key]
        public int GrpIrn { get; set; }
        public string GroupName { get; set; }
        public string MerchantId { get; set; }
        public string MerchantName { get; set; }
        public string Desdcription { get; set; }
        public int CreatedBy { get; set; }
        public int GroupStatus { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
