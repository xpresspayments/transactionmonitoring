﻿using System;
using System.ComponentModel.DataAnnotations;
namespace PosMonitoring.Models
{
    public class MerchantAdmins
    {
        [Key]
        public int ma_id { get; set; }
        public int adminUserId { get; set; }
        public int ma_grpId { get; set; }
        public string ma_mid { get; set; }
        public int ma_createdBy { get; set; }
    }
}
