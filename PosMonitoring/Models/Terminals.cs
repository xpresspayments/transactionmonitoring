﻿using System;
using System.ComponentModel.DataAnnotations;
namespace PosMonitoring.Models
{
    public class Terminals
    {
        [Key]
        public int TrmIrn { get; set; }
        public int TrmGrpIrn { get; set; }
        public string TrmTerminalId { get; set; }
        public int TrmCreatedBy { get; set; }
        public DateTime TrmDateCreated { get; set; }
    }
}
