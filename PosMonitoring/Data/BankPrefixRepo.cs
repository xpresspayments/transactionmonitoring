﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PosMonitoring.AppConstants;
using PosMonitoring.Dtos;
using PosMonitoring.Enums;

namespace PosMonitoring.Data
{
    public class BankPrefixRepo : IBankPrefixRepo
    {
        private readonly string _connectionString;
        private readonly IMapper _mapper;
        private readonly ILogger<TransactionRepo> _logger;

        public BankPrefixRepo(IConfiguration configuration, IMapper mapper, ILogger<TransactionRepo> logger)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _mapper = mapper;
            _logger = logger;

        }

        public async Task<BankPrefixResponse> CreateTerminalPrefix(string TerminalPrefix, long BankId)
        {
            try
            {
                var createPrefixNew = new BankPrefixResponse();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@BankAction", BankPrefix.CREATE_NEW_PREFIX);
                    param.Add("@BankId", BankId);
                    param.Add("@TerminalIdPrefix", TerminalPrefix);

                    int prefixCreated = await _dapper.ExecuteScalarAsync<int>(ApplicationConstant.Sp_BanksDetails, param: param, commandType: CommandType.StoredProcedure);

                    Console.WriteLine("--- Added new prefix ---" + prefixCreated);

                    createPrefixNew.Id = prefixCreated;

                    return createPrefixNew;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: CreateTerminalPrefix(BankPrefixCreateDto TerminalPrefix, BankId) ===>{ex.Message}");
                throw;
            }
        }


        public async Task<BankPrefixReadDto> GetTerminalPrefix(int BankId)
        {
            //throw new NotImplementedException();

            try
            {
                var createPrefixNew = new BankPrefixReadDto();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@BankAction", BankPrefix.GET_PREFIXES);
                    param.Add("@BankId", BankId);

                    var prefixCreated = await _dapper.QueryAsync<BankPrefix>(ApplicationConstant.Sp_BanksDetails, param: param, commandType: CommandType.StoredProcedure);

                    Console.WriteLine("--- Retrieve Prefixes ---" + prefixCreated);

                    return (BankPrefixReadDto)prefixCreated;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: CreateTerminalPrefix(BankPrefixCreateDto TerminalPrefix, BankId) ===>{ex.Message}");
                throw;
            }
        }
    }
}
