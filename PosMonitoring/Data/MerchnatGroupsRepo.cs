﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PosMonitoring.AppConstants;
using PosMonitoring.Dtos;
using PosMonitoring.Enums;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;

namespace PosMonitoring.Data
{
    public class MerchnatGroupsRepo : IMerchantGroups
    {
        private readonly string _connectionString;
        private readonly IMapper _mapper;
        private readonly ILogger<TransactionRepo> _logger;
        private readonly IUriService _uriService;

        public MerchnatGroupsRepo(IConfiguration configuration,
            IUriService uriService,
            IMapper mapper,
            ILogger<TransactionRepo> logger)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _mapper = mapper;
            _logger = logger;
            _uriService = uriService;
        }

        public async Task<MerchantGroupsCreateResponse> CreateNewMerchantGroup(MerchantGroupsCreateDto groupData, string CreatedBy)
        {
            //throw new NotImplementedException();
            try
            {
                var groupCreateResponse = new MerchantGroupsCreateResponse();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@GroupAction", BankPrefix.CREAT_GROUP);
                    param.Add("@GroupName", groupData.GroupName.ToString());
                    param.Add("@MerchantId", groupData.MerchantId.ToString());
                    param.Add("@MerchantName", groupData.MerchantName.ToString());
                    param.Add("@Desdcription", groupData.Desdcription);
                    param.Add("@CreatedBy", CreatedBy);
                    param.Add("@GroupStatus", 0);

                    int groupCreated = await _dapper.ExecuteScalarAsync<int>(ApplicationConstant.Sp_MerchantGroup, param: param, commandType: CommandType.StoredProcedure);

                    Console.WriteLine("--- Merchant Group ID Created ---" + groupCreated);

                    groupCreateResponse.Id = groupCreated;

                    return groupCreateResponse;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: CreateNewMerchantGroup() in repo ===>{ex.Message}");
                throw;
            }
        }

        public async Task<PagedResponse<List<MerchantGroups>>> GetAllGroups(PaginationFilter validFilter, string route)
        {
            //throw new NotImplementedException();

            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {

                    var fetchParam = new DynamicParameters();
                    fetchParam.Add("@GroupAction", BankPrefix.GET_ALL_GROUP);

                    var rResponse = await _dapper.QueryAsync<MerchantGroups>(ApplicationConstant.Sp_MerchantGroup, param: fetchParam, commandType: CommandType.StoredProcedure);

                    var pagedData = rResponse
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                    var totalRecords = rResponse.Count();

                    var pagedReponse = PaginationHelper.CreatePagedReponse<MerchantGroups>(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<MerchantGroups>> response = pagedReponse;
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetAllGroups(PaginationFilter validFilter, string route) ===> In repo" + ex.Message.ToString());
                throw;
            }
        }

        public async Task<MerchantGroups> GetGroupById(string GroupId)
        {
            //throw new NotImplementedException();
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var closeParam = new DynamicParameters();
                    closeParam.Add("@GroupAction", BankPrefix.GET_GROUP);
                    closeParam.Add("@GroupId", Convert.ToInt32(GroupId));
                    var response = await _dapper.QueryFirstOrDefaultAsync<MerchantGroups>(ApplicationConstant.Sp_MerchantGroup, param: closeParam, commandType: CommandType.StoredProcedure);

                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetGroupById() ===> in Repo" + ex.Message.ToString());
                throw;
            }
        }

        public async Task<PagedResponse<List<MerchantGroups>>> GetGroupsByMerchant(PaginationFilter validFilter, string route, string MerchantId)
        {
            //throw new NotImplementedException();

            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {

                    var fetchParam = new DynamicParameters();
                    fetchParam.Add("@GroupAction", BankPrefix.GET_MERCHANT_GROUP);
                    fetchParam.Add("@MerchantId", MerchantId.ToString());

                    var rResponse = await _dapper.QueryAsync<MerchantGroups>(ApplicationConstant.Sp_MerchantGroup, param: fetchParam, commandType: CommandType.StoredProcedure);

                    var pagedData = rResponse
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                    var totalRecords = rResponse.Count();

                    var pagedReponse = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<MerchantGroups>> response = pagedReponse;
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetGroupsByMerchant() ===> In repo" + ex.Message.ToString());
                throw;
            }

        }



        public async Task<MerchantGroups> FindGroupByMerchantAndGroupName(string merchantId, string groupName)
        {
            //throw new NotImplementedException();
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var closeParam = new DynamicParameters();
                    closeParam.Add("@GroupAction", BankPrefix.GET_GROUP_MID_NAME);
                    closeParam.Add("@GroupName", groupName.ToString());
                    closeParam.Add("@MerchantId", merchantId.ToString());

                    var response = await _dapper.QueryFirstOrDefaultAsync<MerchantGroups>(ApplicationConstant.Sp_MerchantGroup, param: closeParam, commandType: CommandType.StoredProcedure);

                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetGroupById() ===> in Repo" + ex.Message.ToString());
                throw;
            }

        }


        //MANAGE ROLES
        public async Task<RoleCreateResponse> CreateNewRole(RoleCreateDto roles, int CreatedBy)
        {
            //throw new NotImplementedException();
            try
            {
                var createRoleResponse = new RoleCreateResponse();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@RoleAction", BankPrefix.ADD_ROLE);
                    param.Add("@RoleAction", roles.RoleName.ToString());
                    param.Add("@RoleCreatedBy", CreatedBy);

                    int newRole = await _dapper.ExecuteScalarAsync<int>(ApplicationConstant.Sp_RolesManagement, param: param, commandType: CommandType.StoredProcedure);

                    Console.WriteLine("--- Role ID Created ---" + newRole);

                    createRoleResponse.RoleCreated = newRole;

                    return createRoleResponse;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: CreateNewRole() in repo ===>{ex.Message}");
                throw;
            }
        }



    }
}
