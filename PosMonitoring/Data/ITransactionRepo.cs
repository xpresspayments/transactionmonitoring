using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PosMonitoring.Dtos;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;

namespace PosMonitoring.Data
{
    public interface ITransactionRepo
    {
        Task<PagedResponse<List<Notification>>> GetPaginatedTransactions(PaginationFilter validFilter, string route, string bankId, int groupId, string accType);
        public Task<DashboardDetailsDto> GetTransactionsAppDashboardTransactions(string bankId, int groupId, string accType);
        Task<PagedResponse<List<Notification>>> PosTransactionsFilter(TransactionFilterDto filterRequest, PaginationFilter validFilter, string route, string userBankId, int groupId, string accType);

        Task<PagedResponse<List<TransactionReadDto>>> GetApprovedTransactions(PaginationFilter validFilter, string route, string bankId, int groupId, string accType);
        Task<PagedResponse<List<TransactionReadDto>>> GetDeclinedTransactions(PaginationFilter validFilter, string route, string bankId, int groupId, string accType);

        Task<TransactionReadDto> GetTransactionById(int id);
        public Task<List<ChartTransactionsRead>> GetChartData(string bankId, int groupId, string accType);

        //Task<PagingResponseModel<List<Notification>>> AlternatePaginatedResult(int currentPageNumber, int pageSize, string route);
    }
}
