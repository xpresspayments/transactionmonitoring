﻿using PosMonitoring.Dtos.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Data.UserAccount.IRepository
{
    public interface IAuditLog
    {
        Task<dynamic> LogActivity(AuditLogDto auditLog);
    }
}
