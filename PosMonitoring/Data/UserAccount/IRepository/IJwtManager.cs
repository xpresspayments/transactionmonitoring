﻿using PosMonitoring.Dtos.Account;
using PosMonitoring.GenericResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PosMonitoring.Data.UserAccount.IRepository
{
    public interface IJwtManager
    {
        Task<AuthResponse> GenerateJsonWebToken(User user);
        Task<AuthResponse> RefreshJsonWebToken(long userId, Claim[] claims);
    }
}
