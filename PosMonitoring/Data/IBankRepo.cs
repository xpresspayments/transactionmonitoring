﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PosMonitoring.Models;
using PosMonitoring.Dtos;
using PosMonitoring.Wrappers;
namespace PosMonitoring.Data
{
    public interface IBankRepo
    {
        Task<IEnumerable<Banks>> GetPaginatedBanksList();

        Task<BankCreateResponse> CreateBanksList(BankCreateDto bank, int bankCreatedBy);

        Task<Banks> FindBankByName(string bankName);

        Task<IEnumerable<Banks>> GetBankWithPrefixList();
       
    }
}
