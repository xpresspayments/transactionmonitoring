﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PosMonitoring.AppConstants;
using PosMonitoring.Dtos;
using PosMonitoring.Enums;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;

namespace PosMonitoring.Data
{
    public class BankRepo: IBankRepo
    {
        private readonly string _connectionString;
        private readonly ILogger<TransactionRepo> _logger;
        private readonly IMapper _mapper;
        private List<object> pagedData;

        public BankRepo(IConfiguration configuration, IMapper mapper, ILogger<TransactionRepo> logger)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _mapper = mapper;
            _logger = logger;

        }

        public async Task<BankCreateResponse> CreateBanksList(BankCreateDto bank, int bankCreatedBy)
        {
                try
                {   
                    var bankCreateResponse = new BankCreateResponse();

                    using (SqlConnection _dapper = new SqlConnection(_connectionString))
                    {
                        var param = new DynamicParameters();
                        param.Add("@BankAction", BankPrefix.CREATE_NEW_BANK);
                        param.Add("@BankName", bank.BankName.ToString());
                        param.Add("@CreatedBy", bankCreatedBy);

                        int isCreated = await _dapper.ExecuteScalarAsync<int>(ApplicationConstant.Sp_BanksDetails, param: param, commandType: CommandType.StoredProcedure);

                        Console.WriteLine("--- Created Bank ---" + isCreated);
                        
                        bankCreateResponse.Id = isCreated;


                        Console.WriteLine("--- Return response on bank create ---" + bankCreateResponse);
                        

                        return bankCreateResponse;

                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"MethodName: CreateBanksList(BankCreateDto bank) ===>{ex.Message}");
                    throw;
                }
        }

        public async Task<Banks> FindBankByName(string bankName)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var closeParam = new DynamicParameters();
                    closeParam.Add("@BankAction", BankPrefix.GET_ONE_BANK);
                    closeParam.Add("@BankName", bankName);
                    var response = await _dapper.QueryFirstOrDefaultAsync<Banks>(ApplicationConstant.Sp_BanksDetails, param: closeParam, commandType: CommandType.StoredProcedure);

                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: FindBankByName() ===>" + ex.Message.ToString());
                throw;
            }

        }

        public async Task<IEnumerable<Banks>> GetPaginatedBanksList()
        {

            //throw new NotImplementedException();
            try
            {
                //var request = InputFormatter(filterRequest);

                var closeParam = new DynamicParameters();
                closeParam.Add("@BankAction", BankPrefix.GET_ALL_BANKS);

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {

                    dynamic rspParam = await _dapper.QueryAsync<Banks>(ApplicationConstant.Sp_BanksDetails, param: closeParam, commandType: CommandType.StoredProcedure);

                    return rspParam;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetPaginatedBanksList() ===>" + ex.Message.ToString());
                throw;
            }
            
        }

        public async Task<IEnumerable<Banks>> GetBankWithPrefixList()
        {

            //throw new NotImplementedException();
            try
            {
                //var request = InputFormatter(filterRequest);

                var closeParam = new DynamicParameters();
                closeParam.Add("@BankAction", BankPrefix.GET_PREFIXES);

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {

                    dynamic rspParam = await _dapper.QueryAsync<Banks>(ApplicationConstant.Sp_BanksDetails, param: closeParam, commandType: CommandType.StoredProcedure);

                    return rspParam;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetPaginatedBanksList() ===>" + ex.Message.ToString());
                throw;
            }

        }

        
    }

    
}
