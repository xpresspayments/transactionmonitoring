﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PosMonitoring.AppConstants;
using PosMonitoring.Dtos;
using PosMonitoring.Dtos.Account;
using PosMonitoring.Enums;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;
using PosMonitoring.Data.UserAccount.IRepository;

namespace PosMonitoring.Data
{
    public class TerminalsRepo : ITerminalsRepo
    {
        private readonly string _connectionString;
        private readonly IMapper _mapper;
        private readonly ILogger<TerminalsReadDto> _logger;
        private readonly IUriService _uriService;
        private readonly IAccountRepository _userAccountRepo;

        public TerminalsRepo(IConfiguration configuration,
            IUriService uriService,
            IMapper mapper,
            ILogger<TerminalsReadDto> logger,
            IAccountRepository userAccount
            )
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _mapper = mapper;
            _logger = logger;
            _uriService = uriService;
            _userAccountRepo = userAccount;
        }

        public async Task<TerminalsCreateResponse> AddTerminalToGroup(TerminalsCreateDto termsData, string addedBy)
        {
            //throw new NotImplementedException();
            try
            {
                var terminalCreateResponse = new TerminalsCreateResponse();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@TerminalAction", BankPrefix.ASSIGN_TID_TO_GROUP);
                    param.Add("@GroupId", Convert.ToInt32(termsData.TrmGrpIrn));
                    param.Add("@TerminalId", termsData.TrmTerminalId.ToString());
                    param.Add("@CreatedBy", Convert.ToInt32(addedBy));

                    int groupCreated = await _dapper.ExecuteScalarAsync<int>(ApplicationConstant.Sp_MerchantGroup, param: param, commandType: CommandType.StoredProcedure);


                    terminalCreateResponse.Id = groupCreated;

                    return terminalCreateResponse;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: AddTerminalToGroup() in repo ===>{ex.Message}");
                throw;
            }
        }

        public async Task<UserAssignToGroupResponse> AssignUserToGroup(GroupAsignUserCreateDto UserAsignData, string asignBy, string userId)
        {
            try
            {
                var userAssignToGroupResponse = new UserAssignToGroupResponse();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@TerminalAction", BankPrefix.ASIGN_USER_TO_GROUP);
                    param.Add("@GroupId", UserAsignData.GroupId);
                    param.Add("@UserId", Convert.ToInt32(userId));

                    var userAsigned = await _dapper.ExecuteAsync(ApplicationConstant.Sp_MerchantGroup, param: param, commandType: CommandType.StoredProcedure);

                    userAssignToGroupResponse.UserId = userAsigned;

                    return userAssignToGroupResponse;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: AssignUserToGroup() in repo ===>{ex.Message}");
                throw;
            }
        }


        public async Task<GroupToMerchantAdminResponse> AssignGroupToMerchantAdmin(GroupAsignToMerchantAdminDto maGroup, long maId, string userId)
        {
            try
            {
                var asignGrpToMa = new GroupToMerchantAdminResponse();

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@TerminalAction", BankPrefix.ASIGN_GRP_TO_MA);
                    param.Add("@ma_grpId", maGroup.groupId);
                    param.Add("@ma_mid", maGroup.grpMid); 
                    param.Add("@adminUserId", Convert.ToInt32(maId));
                    param.Add("@ma_createdBy", Convert.ToInt32(userId));

                    var userAsigned = await _dapper.ExecuteAsync(ApplicationConstant.Sp_MerchantGroup, param: param, commandType: CommandType.StoredProcedure);

                    asignGrpToMa.ma_Id = userAsigned;

                    return asignGrpToMa;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: AssignGroupToMerchantAdmin() in repo ===>{ex.Message}");
                throw;
            }
        }

        public async Task<Terminals> FindByTerminalId(string tid, int groupId)
        {
            //throw new NotImplementedException();
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var closeParam = new DynamicParameters();
                    closeParam.Add("@TerminalAction", BankPrefix.GET_ONE_TERMINAL);
                    closeParam.Add("@TerminalId", tid);
                    closeParam.Add("@GroupId", groupId);
                    var response = await _dapper.QueryFirstOrDefaultAsync<Terminals>(ApplicationConstant.Sp_MerchantGroup, param: closeParam, commandType: CommandType.StoredProcedure);

                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: FindByTerminalId() ===> in Repo" + ex.Message.ToString());
                throw;
            }
        }

        public async Task<PagedResponse<List<Terminals>>> GetAllTerminals(PaginationFilter validFilter, string route)
        {
            //throw new NotImplementedException();
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {

                    var fetchParam = new DynamicParameters();
                    fetchParam.Add("@TerminalAction", BankPrefix.GET_ALL_TIDS);

                    var rResponse = await _dapper.QueryAsync<Terminals>(ApplicationConstant.Sp_MerchantGroup, param: fetchParam, commandType: CommandType.StoredProcedure);

                    var pagedData = rResponse
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                    var totalRecords = rResponse.Count();

                    var pagedReponse = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<Terminals>> response = pagedReponse;
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetAllTerminals(PaginationFilter validFilter, string route) ===> In repo" + ex.Message.ToString());
                throw;
            }
        }
    }
}
