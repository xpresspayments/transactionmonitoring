﻿using System;
using System.Threading.Tasks;

namespace PosMonitoring.Data.Schedular
{
    public interface IFetchData
    {
        Task GetSaveMerchants();

        Task GetSaveTerminals();
    }
}
