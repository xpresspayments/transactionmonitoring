﻿using System;
using System.Threading.Tasks;
using PosMonitoring.Dtos;
namespace PosMonitoring.Data
{
    public interface IBankPrefixRepo
    {
        Task<BankPrefixResponse> CreateTerminalPrefix(string TerminalPrefix, long BankId);

        Task<BankPrefixReadDto> GetTerminalPrefix(int BankId);
    }
}
