﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PosMonitoring.AppConstants;
using PosMonitoring.Data.UserAccount.IRepository;
using PosMonitoring.Dtos.Account;
using PosMonitoring.Enums;
using PosMonitoring.GenericResponse;
using PosMonitoring.Validators;
using PosMonitoring.ViewModels;
using PosMonitoring.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PosMonitoring.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<AuthController> _logger;
        private readonly IAccountRepository _accountRepository;
        private readonly IJwtManager _jwtManager;
        private readonly ITokenRefresher _tokenRefresher;
        private readonly IAuditLog _audit;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IUriService _uriService;
        private int MaxNumberOfFailedAttemptsToLogin;
        private int MinutesBeforeResetAfterFailedAttemptsToLogin;
        private int CharacterLengthMax;
        private int CharacterLengthMin;
        private int MustContainUppercase;
        private int MustContainLowercase;
        private int MustContainNumber;

        public AuthController(ITokenRefresher tokenRefresher, IUnitOfWork unitOfWork,
            IAuditLog audit, IMapper mapper, IJwtManager jwtManager, IWebHostEnvironment hostEnvironment,
            IAccountRepository accountRepository, ILogger<AuthController> logger, IUriService uriService)
        {
            _tokenRefresher = tokenRefresher;
            _unitOfWork = unitOfWork;
            _audit = audit;
            _mapper = mapper;
            _jwtManager = jwtManager;
            _logger = logger;
            _accountRepository = accountRepository;
            _hostEnvironment = hostEnvironment;
            _uriService = uriService;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        [ValidateModel]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            var response = new LoginResponse();
            try
            {
                //var Un = Convert.ToBase64String(Encoding.UTF8.GetBytes(Convert.ToString(login.Email)));
                //var pw = Convert.ToBase64String(Encoding.UTF8.GetBytes(Convert.ToString(login.Password)));

                var email = Encoding.UTF8.GetString(Convert.FromBase64String(login.Email));
                var password = Encoding.UTF8.GetString(Convert.FromBase64String(login.Password));

                var hashPassword = Utils.HashPassword(password);

                var decodedLogin = new LoginModel { Email = email, Password = hashPassword };

                var payload = JsonConvert.SerializeObject(decodedLogin);
                var ipAddress = Request.HttpContext.Connection.RemoteIpAddress?.ToString();

                var user = await _accountRepository.FindUser(Encoding.UTF8.GetString(Convert.FromBase64String(login.Email)));

                if (user == null)
                {
                    response.ResponseCode = ResponseCode.ValidationError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "No user account found with provided email";
                    return Ok(response);
                }

                if (user.IsLogin && user.LoggedInWithIPAddress == ipAddress)
                {
                    response.ResponseCode = ResponseCode.InvalidPassword.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = $"You are already logged in on this PC.";
                    return Ok(response);
                }

                if (user.IsLogin && user.LoggedInWithIPAddress != ipAddress)
                {
                    response.ResponseCode = ResponseCode.InvalidPassword.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = $"You are already logged in on another PC.";
                    return Ok(response);
                }

                var systemConfig = await _unitOfWork.SystemConfiguration();

                foreach (var config in systemConfig)
                {
                    if (config.Name.ToLower() == "maxnumberoffailedattemptstologin")
                        MaxNumberOfFailedAttemptsToLogin = config.Value;
                    if (config.Name.ToLower() == "minutesbeforeresetafterfailedattemptstologin")
                        MinutesBeforeResetAfterFailedAttemptsToLogin = config.Value;
                }


                if (user != null)
                {
                    if (user.IsDeactivated)
                    {
                        response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "You have been deactivated please contact admin";

                        var LogDeactivatedAccount = new AuditLogDto
                        {
                            userId = user.UserId,
                            actionPerformed = "Login",
                            payload = payload,
                            response = JsonConvert.SerializeObject(response),
                            actionStatus = $"Unauthorized: {response.ResponseMessage}",
                            ipAddress = ipAddress
                        };

                        await _audit.LogActivity(LogDeactivatedAccount);

                        return Ok(response);

                    }

                    if (user.LoginFailedAttemptsCount >= MaxNumberOfFailedAttemptsToLogin
                        && user.LastLoginAttemptAt.HasValue
                        && DateTime.Now < user.LastLoginAttemptAt.Value.AddMinutes(MinutesBeforeResetAfterFailedAttemptsToLogin))
                    {
                        response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "You account was blocked, please contact admin";

                        var LogBlockedAccount = new AuditLogDto
                        {
                            userId = user.UserId,
                            actionPerformed = "Login",
                            payload = payload,
                            response = JsonConvert.SerializeObject(response),
                            actionStatus = $"Unauthorized: {response.ResponseMessage}",
                            ipAddress = ipAddress
                        };

                        await _audit.LogActivity(LogBlockedAccount);

                        return Ok(response);
                    }


                    if (user.IsActive)
                    {
                        var isPasswordMatch = Utils.DoesPasswordMatch(user.PasswordHash, Encoding.UTF8.GetString(Convert.FromBase64String(login.Password)));
                        if (isPasswordMatch)
                        {

                            var authResponse = await _jwtManager.GenerateJsonWebToken(user);

                            await _unitOfWork.UpdateUserLoginActivity(user.UserId, ipAddress, authResponse.JwtToken);

                            var mapped = _mapper.Map<UserViewModel>(user);


                            response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                            response.ResponseMessage = "User Logged in Successfully";
                            response.JwtToken = authResponse.JwtToken;
                            response.RefreshToken = authResponse.RefreshToken;
                            response.Data = mapped;

                            var LogSuccessfulLogin = new AuditLogDto
                            {
                                userId = user.UserId,
                                actionPerformed = "Login",
                                payload = payload,
                                response = JsonConvert.SerializeObject(response),
                                actionStatus = $"Successful: {response.ResponseMessage}",
                                ipAddress = ipAddress
                            };

                            await _audit.LogActivity(LogSuccessfulLogin);

                            return Ok(response);
                        }

                        var attemptCount = user.LoginFailedAttemptsCount + 1;
                        await _unitOfWork.UpdateLastLoginAttempt(attemptCount, user.Email);

                        if (attemptCount >= MaxNumberOfFailedAttemptsToLogin)
                        {
                            response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                            response.ResponseMessage = $"You have exceeded number of attempts. your account has been locked. Please contact admin.";

                            var LogLockedAccount = new AuditLogDto
                            {
                                userId = user.UserId,
                                actionPerformed = "Login",
                                payload = payload,
                                response = JsonConvert.SerializeObject(response),
                                actionStatus = $"Failed: {response.ResponseMessage}",
                                ipAddress = ipAddress
                            };

                            await _audit.LogActivity(LogLockedAccount);

                            return Ok(response);
                        }

                        response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = $"Invalid Password! You have made {attemptCount} unsuccessful attempt(s). " +
                                                   $"The maximum retry attempts allowed is {MaxNumberOfFailedAttemptsToLogin}. " +
                                                   $"If {MaxNumberOfFailedAttemptsToLogin} is exceeded, then you will be locked out of the system";

                        var LogInvalidPw = new AuditLogDto
                        {
                            userId = user.UserId,
                            actionPerformed = "Login",
                            payload = payload,
                            response = JsonConvert.SerializeObject(response),
                            actionStatus = $"Failed: {response.ResponseMessage}",
                            ipAddress = ipAddress
                        };
                        await _audit.LogActivity(LogInvalidPw);

                        return Ok(response);
                    }
                    response.ResponseCode = ResponseCode.AuthorizationError.ToString("D");
                    response.ResponseMessage = "User is inactive";

                    var LogInactiveUserTrail = new AuditLogDto
                    {
                        userId = user.UserId,
                        actionPerformed = "Login",
                        payload = payload,
                        response = JsonConvert.SerializeObject(response),
                        actionStatus = $"Failed: {response.ResponseMessage}",
                        ipAddress = ipAddress
                    };
                    await _audit.LogActivity(LogInactiveUserTrail);


                    return Ok(response);
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : Login ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : Login ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }
        }

        [AllowAnonymous]
        [ValidateModel]
        [HttpPost("RefreshToken")]
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenModel refresh)
        {
            //use fv to validate RefreshTokenModel input
            var response = new RefreshTokenResponse();
            try
            {
                var token = await _tokenRefresher.Refresh(refresh);

                if (token == null)
                {
                    response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Invalid refresh token";
                    return Ok(response);
                }

                response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Token refreshed successfully";
                response.JwtToken = token.JwtToken;
                response.RefreshToken = token.RefreshToken;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : Refresh ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : Refresh ==> {ex.Message}";
                return Ok(response);
            }
        }

        [HttpPost("Logout")]
        [ValidateModel]
        public async Task<IActionResult> Logout(LogoutDto logout)
        {
            var response = new ApiResponse();
            try
            {
                await _unitOfWork.UpdateLogout(Encoding.UTF8.GetString(Convert.FromBase64String(logout.Email)));

                response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Logged out successfully";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : Logout ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : Logout ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }
        }

        [HttpPost("CreateUser")]
        [ValidateModel]
        [Authorize]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserDto userDto)
        {
            var response = new ApiResponse();
            try
            {
                string createdbyUserEmail = this.User.Claims.ToList()[2].Value;
                string createdbyUserId = this.User.Claims.ToList()[3].Value;

                //var ipAddress = Request.HttpContext.Connection.LocalIpAddress?.ToString();
                //var port = Request.HttpContext.Connection.LocalPort.ToString();

                var isExists = await _accountRepository.FindUser(userDto.Email);
                if (null != isExists)
                {
                    response.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "User with email already exists.";
                    return Ok(response);
                }

                //Console.WriteLine("--- PARAMETERS TO CREATE NEW USER ---" + userDto);

                var resp = await _accountRepository.AddUser(userDto, Convert.ToInt32(createdbyUserId));

                if (resp != null && resp.IsCreated > 0)
                {
                    var user = await _accountRepository.FindUser(userDto.Email);

                    // _accountRepository.SendEmail(userDto.Email, userDto.FirstName, defaultPassword, "Activation Email", _hostEnvironment.ContentRootPath, ipAddress, port);
                    response.Data = user;
                    response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "User created successfully.";
                    return Ok(response);
                }

                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "An error occured while Creating user. Please contact admin.";
                response.Data = resp;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : CreateUser ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : CreateUser ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }


        }

        [HttpPut("UpdateUser")]
        [ValidateModel]
        [Authorize]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserDto updateDto)
        {
            var response = new ApiResponse();
            string requesterUserId = this.User.Claims.ToList()[3].Value;
            try
            {
                var user = await _accountRepository.FindUser(updateDto.Email);
                if (null != user)
                {
                    dynamic resp = await _accountRepository.UpdateUser(updateDto, Convert.ToInt32(requesterUserId));
                    if (resp > 0)
                    {
                        var updatedUser = await _accountRepository.FindUser(updateDto.Email);
                        var mapped = _mapper.Map<UserViewModel>(updatedUser);

                        _logger.LogInformation("User updated successfully.");
                        response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "User updated successfully.";
                        response.Data = mapped;
                        return Ok(response);

                    }
                    response.ResponseCode = ResponseCode.Exception.ToString();
                    response.ResponseMessage = "An error occurred while updating user.";
                    response.Data = null;
                    return Ok(response);
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "No record found for the specified Username";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : UpdateUserDto ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : UpdateUserDto ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }

        }

        [HttpPost("SendEmailForPasswordChange")]
        [AllowAnonymous]
        [ValidateModel]
        [Authorize]
        public async Task<IActionResult> SendEmailForPasswordChange(RequestPasswordChange request)
        {
            var response = new ApiResponse();
            var ipAddress = Request.HttpContext.Connection.LocalIpAddress?.ToString();
            var port = Request.HttpContext.Connection.LocalPort.ToString();

            //var email = Encoding.UTF8.GetString(Convert.FromBase64String(request.Email));

            try
            {
                var user = await _accountRepository.FindUser(request.Email);
                if (null == user)
                {
                    response.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Invalid email. Please provide a valid email";
                    return Ok(response);
                }

                var password = Utils.RandomPassword();
                user.PasswordHash = Utils.HashPassword(password);
                _accountRepository.SendEmail(user.Email, user.FirstName, password, "Password Reset", _hostEnvironment.ContentRootPath, ipAddress, port);

                response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "A link has been sent to your email to reset your password. Use "+password+ " To login if link not received";
                return Ok(response);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : SendEmailForPasswordChange ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : SendEmailForPasswordChange ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }


        }

        [HttpPost("ChangePassword")]
        [AllowAnonymous]
        [ValidateModel]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel changePassword)
        {
            var response = new ApiResponse();

            var npw = Convert.ToBase64String(Encoding.UTF8.GetBytes(Convert.ToString("Password1234")));
            string email = Encoding.UTF8.GetString(Convert.FromBase64String(changePassword.Email));
            string oldPassword = Encoding.UTF8.GetString(Convert.FromBase64String(changePassword.OldPassword));
            string newPassword = Encoding.UTF8.GetString(Convert.FromBase64String(changePassword.NewPassword));

            try
            {
                var user = await _accountRepository.FindUser(email);
                if (null == user)
                {
                    response.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Invalid email. Please provide a valid email";
                    return Ok(response);
                }

                var systemConfig = await _unitOfWork.SystemConfiguration();

                foreach (var config in systemConfig)
                {
                    if (config.Name.ToLower() == "maxnumberoffailedattemptstologin")
                        MaxNumberOfFailedAttemptsToLogin = config.Value;
                    if (config.Name.ToLower() == "minutesbeforeresetafterfailedattemptstologin")
                        MinutesBeforeResetAfterFailedAttemptsToLogin = config.Value;
                    if (config.Name.ToLower() == "characterlengthmax")
                        CharacterLengthMax = config.Value;
                    if (config.Name.ToLower() == "characterlengthmin")
                        CharacterLengthMin = config.Value;
                    if (config.Name.ToLower() == "mustcontainuppercase")
                        MustContainUppercase = config.Value;
                    if (config.Name.ToLower() == "mustcontainlowercase")
                        MustContainLowercase = config.Value;
                    if (config.Name.ToLower() == "mustcontainnumber")
                        MustContainNumber = config.Value;
                }

                if (user.LoginFailedAttemptsCount >= MaxNumberOfFailedAttemptsToLogin)
                {
                    response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "You account was blocked, please contact admin";
                    return Ok(response);
                }

                var isPasswordMatch = Utils.DoesPasswordMatch(user.PasswordHash, oldPassword);
                if (isPasswordMatch)
                {
                    var hasNumber = new Regex(@"[0-9]+");
                    var hasUpperChar = new Regex(@"[A-Z]+");
                    var hasLowerChar = new Regex(@"[a-z]+");
                    var hasMinChars = new Regex(@".{" + CharacterLengthMin + ",}");
                    var hasMaxChars = new Regex(@".{" + CharacterLengthMax + ",}");

                    bool isValidatedMin = hasMinChars.IsMatch(newPassword);
                    bool isValidatedMax = hasMaxChars.IsMatch(newPassword);
                    if (isValidatedMin && !isValidatedMax)
                    {
                        if (MustContainNumber == 1) // true
                        {
                            if (!hasNumber.IsMatch(newPassword))
                            {
                                response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                                response.ResponseMessage = "Password must contain a numeric value";
                                return Ok(response);
                            }
                        }
                        if (MustContainUppercase == 1) // true
                        {
                            if (!hasUpperChar.IsMatch(newPassword))
                            {
                                response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                                response.ResponseMessage = "Password must contain upper case character";
                                return Ok(response);
                            }
                        }
                        if (MustContainLowercase == 1) // true
                        {
                            if (!hasLowerChar.IsMatch(newPassword))
                            {
                                response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                                response.ResponseMessage = "Password must contain lower case character";
                                return Ok(response);
                            }
                        }

                        if (!user.IsActive)
                            user.IsActive = true;


                        var resp = await _accountRepository.ChangePassword(user.UserId, newPassword);
                        if (resp > 0)
                        {
                            response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                            response.ResponseMessage = "Password changed successfully.";
                            return Ok(response);
                        }

                        response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "An error occured while updating your password. Please contact admin.";
                        return Ok(response);
                    }
                    else
                    {
                        response.ResponseCode = ResponseCode.AuthorizationError.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = $"Password must be between {CharacterLengthMin} and {CharacterLengthMax} characters";
                        return Ok(response);
                    }
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Old password is not valid";
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : ChangePassword ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : ChangePassword ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }

        }

        [Authorize]
        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers([FromQuery] PaginationFilter filter)
        {
            PagedResponse<List<UserViewModel>> response = null;
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            try
            {
                var systemConfig = await _unitOfWork.SystemConfiguration();

                foreach (var config in systemConfig)
                {
                    if (config.Name.ToLower() == "maxnumberoffailedattemptstologin")
                        MaxNumberOfFailedAttemptsToLogin = config.Value;
                    if (config.Name.ToLower() == "minutesbeforeresetafterfailedattemptstologin")
                        MinutesBeforeResetAfterFailedAttemptsToLogin = config.Value;
                }

                var mappeduser = new List<UserViewModel>();
                var users = await _accountRepository.GetAllUsers();
                if (users.Any())
                {
                    foreach (var user in users)
                    {

                        var usermap = _mapper.Map<UserViewModel>(user);
                        usermap.IsLockedOut = user.LoginFailedAttemptsCount >= MaxNumberOfFailedAttemptsToLogin;
                        if (usermap.IsLockedOut)
                            usermap.UserStatus = "LockedOut";
                        else if (usermap.IsDeactivated)
                            usermap.UserStatus = "Deactivated";
                        else 
                            usermap.UserStatus = user.IsApproved == true ? "Approved" : user.IsApproved == false && user.IsDisapproved == true ? "Disapproved" : "Pending";

                        mappeduser.Add(usermap);
                    }                   
                }
                else
                {
                    response.Succeeded = true;
                    response.Message = "No Users found.";
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.Errors = null;
                    return Ok(response);
                }
                var pagedData = mappeduser
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                var totalRecords = mappeduser.Count();

                var pagedReponse = PaginationHelper.CreatePagedReponse<UserViewModel>(pagedData, validFilter, totalRecords, _uriService, route);

                response = pagedReponse;

                response.Succeeded = true;
                response.Message = "Users fetched successfully.";
                response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                response.Errors = null;

                return Ok(response);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetAllUsersPendingApproval ==> {ex.Message}");
                var Error = new[] { ex.Message };
                return Ok(new Response<PagedResponse>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [Authorize]
        [HttpGet("GetAllUsersPendingApproval")]
        public async Task<IActionResult> GetAllUsersPendingApproval([FromQuery] PaginationFilter filter)
        {
            PagedResponse<List<UserViewModel>> response = null;
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

            try
            {
                var mappeduser = new List<UserViewModel>();
                var users = await _accountRepository.GetUsersPendingApproval();
                if (users.Any())
                {
                    foreach (var user in users)
                    {
                        var usermap = _mapper.Map<UserViewModel>(user);
                        mappeduser.Add(usermap);
                    }
                }
                else
                {
                    response.Succeeded = true;
                    response.Message = "No Users found.";
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.Errors = null;
                    return Ok(response);
                }
                var pagedData = mappeduser
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                var totalRecords = mappeduser.Count();

                var pagedReponse = PaginationHelper.CreatePagedReponse<UserViewModel>(pagedData, validFilter, totalRecords, _uriService, route);

                response = pagedReponse;

                response.Succeeded = true;
                response.Message = "Users fetched successfully.";
                response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                response.Errors = null;

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetAllUsersPendingApproval ==> {ex.Message}");
                var Error = new[] { ex.Message };
                return Ok(new Response<PagedResponse>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpPut("ApproveUser")]
        [Authorize]
        [ValidateModel]
        public async Task<IActionResult> ApproveUser(ApproveUserDto approveUser)
        {
            var response = new ApiResponse();
            try
            {
                var ipAddress = Request.HttpContext.Connection.LocalIpAddress?.ToString();
                var port = Request.HttpContext.Connection.LocalPort.ToString();

                string requesterUserEmail = this.User.Claims.ToList()[2].Value;
                string requesterUserId = this.User.Claims.ToList()[3].Value;

                var requesterInfo = await _accountRepository.FindUser(requesterUserEmail);
                if (null == requesterInfo)
                {
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Requester information cannot be found.";
                    return Ok(response);
                }

                if (requesterInfo.RoleName.ToLower() == "user" || requesterInfo.RoleId == 2) //update user-RoleId here when roles have been added
                {
                    response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "You do not have permission to perform this request.";
                    return Ok(response);
                }

                var user = await _accountRepository.FindUser(approveUser.Email);
                if (user != null)
                {
                    if (user.CreatedByUserId == Convert.ToInt32(requesterUserId))
                    {
                        response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "You cannot approve this User because User was created by you.";
                        return Ok(response);
                    }
                    string defaultPass = Utils.RandomPassword();
                    dynamic resp = await _accountRepository.ApproveUser(Convert.ToInt32(requesterUserId), defaultPass, user.Email);
                    if (resp > 0)
                    {
                        _logger.LogInformation($"User with email: {user.Email} approved successfully.");
                        //_accountRepository.SendEmail(user.Email, user.FirstName, defaultPass, "Activation Email", _hostEnvironment.ContentRootPath, ipAddress, port);
                        response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = $"User with email: {user.Email} approved successfully. Use default password "+ defaultPass+" If email not delivered";
                        return Ok(response);
                    }
                    response.ResponseCode = ResponseCode.Exception.ToString();
                    response.ResponseMessage = "An error occurred while approving the user.";
                    return Ok(response);
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Invalid user. Not found.";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : ApproveUser ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : ApproveUser ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }

        }

        [HttpPut("DisapproveUser")]
        [Authorize]
        [ValidateModel]
        public async Task<IActionResult> DisapproveUser(DisapproveUserDto disapproveUser)
        {
            var response = new ApiResponse();
            try
            {
                string requesterUserEmail = this.User.Claims.ToList()[2].Value;
                string requesterUserId = this.User.Claims.ToList()[3].Value;

                var requesterInfo = await _accountRepository.FindUser(requesterUserEmail);
                if (null == requesterInfo)
                {
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Requester information cannot be found.";
                    return Ok(response);
                }

                if (requesterInfo.RoleName.ToLower() == "user" || requesterInfo.RoleId == 2) //update user-RoleId here when roles have been added
                {
                    response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "You do not have permission to perform this request.";
                    return Ok(response);
                }

                var user = await _accountRepository.FindUser(disapproveUser.Email);
                if (user != null)
                {
                    if (user.CreatedByUserId == Convert.ToInt32(requesterUserId))
                    {
                        response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "You cannot disapprove this User because User was created by you.";
                        return Ok(response);
                    }
                    dynamic resp = await _accountRepository.DeclineUser(Convert.ToInt32(requesterUserId), user.Email, disapproveUser.DisapprovedComment);
                    if (resp > 0)
                    {
                        _logger.LogInformation($"User with email: {user.Email} disapproved successfully.");
                        response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = $"User with email: {user.Email} disapproved successfully.";
                        return Ok(response);
                    }
                    response.ResponseCode = ResponseCode.Exception.ToString();
                    response.ResponseMessage = "An error occurred while disapproving the user.";
                    return Ok(response);
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Invalid user. Not found.";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : DisapproveUser ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : DisapproveUser ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }

        }

        [HttpPost("DeactivateUser")]
        [ValidateModel]
        [Authorize]
        public async Task<IActionResult> DeactivateUser(DeactivateUserDto deactivateUser)
        {
            var response = new ApiResponse();
            try
            {
                string requesterUserEmail = this.User.Claims.ToList()[2].Value;
                string requesterUserId = this.User.Claims.ToList()[3].Value;

                var requesterInfo = await _accountRepository.FindUser(requesterUserEmail);
                if (null == requesterInfo)
                {
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Requester information cannot be found.";
                    return Ok(response);
                }

                if (requesterInfo.RoleName.ToLower() == "user" || requesterInfo.RoleId == 2) //update user-RoleId here when roles have been added
                {
                    response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "You do not have permission to perform this request.";
                    return Ok(response);
                }

                var user = await _accountRepository.FindUser(deactivateUser.Email);
                if (user != null)
                {
                    dynamic resp = await _accountRepository.DeactivateUser(Convert.ToInt32(requesterUserId), user.Email, deactivateUser.DeactivatedComment);
                    if (resp > 0)
                    {
                        _logger.LogInformation($"User with email: {user.Email} deactivated successfully.");
                        response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = $"User with email: {user.Email} deactivated successfully.";
                        return Ok(response);
                    }
                    response.ResponseCode = ResponseCode.Exception.ToString();
                    response.ResponseMessage = "An error occurred while deactivating the user.";
                    return Ok(response);
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Invalid user. Not found.";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : DeactivateUser ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : DeactivateUser ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }
        }

        [HttpPost("Re-activateUser")]
        [ValidateModel]
        [Authorize]
        public async Task<IActionResult> ReactivateUser(ReactivateUserDto reactivateUser)
        {
            var response = new ApiResponse();
            try
            {
                string requesterUserEmail = this.User.Claims.ToList()[2].Value;
                string requesterUserId = this.User.Claims.ToList()[3].Value;

                var requesterInfo = await _accountRepository.FindUser(requesterUserEmail);
                if (null == requesterInfo)
                {
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Requester information cannot be found.";
                    return Ok(response);
                }

                if (requesterInfo.RoleName.ToLower() == "user" || requesterInfo.RoleId == 2) //update user-RoleId here when roles have been added
                {
                    response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "You do not have permission to perform this request.";
                    return Ok(response);
                }
                
                var user = await _accountRepository.FindUser(reactivateUser.Email);
                if (user != null)
                {
                    dynamic resp = await _accountRepository.ReactivateUser(Convert.ToInt32(requesterUserId), user.Email, reactivateUser.ReactivatedComment);
                    if (resp > 0)
                    {
                        _logger.LogInformation($"User with email: {user.Email} reactivated successfully.");
                        response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = $"User with email: {user.Email} reactivated successfully.";
                        return Ok(response);
                    }
                    response.ResponseCode = ResponseCode.Exception.ToString();
                    response.ResponseMessage = "An error occurred while reactivating the user.";
                    return Ok(response);
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Invalid user. Not found.";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : ReactivateUser ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : ReactivateUser ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }            
        }

        [HttpPost("UnblockAccount")]
        [Authorize]
        [ValidateModel]
        public async Task<IActionResult> UnblockAccount(UnblockAccountDto unblockUser)
        {
            var response = new ApiResponse();
            try
            {
                var ipAddress = Request.HttpContext.Connection.LocalIpAddress?.ToString();
                var port = Request.HttpContext.Connection.LocalPort.ToString();

                string requesterUserEmail = this.User.Claims.ToList()[2].Value;
                string requesterUserId = this.User.Claims.ToList()[3].Value;

                var requesterInfo = await _accountRepository.FindUser(requesterUserEmail);
                if (null == requesterInfo)
                {
                    response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "Requester information cannot be found.";
                    return Ok(response);
                }

                if (requesterInfo.RoleName.ToLower() == "user" || requesterInfo.RoleId == 2) //update user-RoleId here when roles have been added
                {
                    response.ResponseCode = ResponseCode.ProcessingError.ToString("D").PadLeft(2, '0');
                    response.ResponseMessage = "You do not have permission to perform this request.";
                    return Ok(response);
                }

                var user = await _accountRepository.FindUser(unblockUser.Email);
                if (user != null)
                {
                    string defaultPass = Utils.RandomPassword();
                    dynamic resp = await _accountRepository.UnblockUser(Convert.ToInt32(requesterUserId), defaultPass, user.Email);
                    if (resp > 0)
                    {
                        _logger.LogInformation($"User with email: {user.Email} unblocked successfully.");
                        _accountRepository.SendEmail(user.Email, user.FirstName, defaultPass, "Unblock Account", _hostEnvironment.ContentRootPath, ipAddress, port);
                        response.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                        response.ResponseMessage = "A link has been sent to the user to reset their password";
                        return Ok(response);
                    }
                    response.ResponseCode = ResponseCode.Exception.ToString();
                    response.ResponseMessage = "An error occurred while unblocking the user.";
                    return Ok(response);
                }
                response.ResponseCode = ResponseCode.NotFound.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = "Invalid user. Not found.";
                response.Data = null;
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : unblockUser ==> {ex.Message}");
                response.ResponseCode = ResponseCode.Exception.ToString("D").PadLeft(2, '0');
                response.ResponseMessage = $"Exception Occured: ControllerMethod : unblockUser ==> {ex.Message}";
                response.Data = null;
                return Ok(response);
            }
        }

    }
}
