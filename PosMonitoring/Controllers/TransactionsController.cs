﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PosMonitoring.AppConstants;
using PosMonitoring.Data;
using PosMonitoring.Dtos;
using PosMonitoring.Enums;
using PosMonitoring.GenericResponse;
using PosMonitoring.Models;
using PosMonitoring.SyncDataServices.Http;
using PosMonitoring.Wrappers;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace PosMonitoring.Controllers
{
    [EnableCors("AllowAnyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionRepo _repository;
        private readonly IMapper _mapper;
        private readonly ICommandDataClient _commandDataClient;
        private readonly ILogger<TransactionsController> _logger;
        private readonly IBankRepo _bankRepo;
        private readonly IBankPrefixRepo _prefixRepo;
        private readonly string _connectionString;
        private readonly IUriService _uriService;

        public TransactionsController(
            ITransactionRepo repository,
            ILogger<TransactionsController> logger,
            IMapper mapper,
            ICommandDataClient commandDataClient,
            IBankRepo bankRepo,
            IBankPrefixRepo prefixRepo,
            IConfiguration configuration,
            IUriService uriService
            )
        {
            _repository = repository;
            _mapper = mapper;
            _commandDataClient = commandDataClient;
            _logger = logger;
            _bankRepo = bankRepo;
            _prefixRepo = prefixRepo;
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _uriService = uriService;
        }

        [HttpGet("GetTransactionById/{id}", Name = "GetTransactionById")]
        public async Task<ActionResult<TransactionReadDto>> GetTransactionById(int id)
        {
            var getTxnResult = await _repository.GetTransactionById(id);

            if (null != getTxnResult)
            {
                return Ok(_mapper.Map<TransactionReadDto>(getTxnResult));
            }

            return NotFound();

        }


        [HttpGet("GetPaginatedTransactionList")]
        public IActionResult AlternatePaginatedResult(int pageNumber, int pageSize)
        {
            int maxPagSize = 50;
            pageSize = (pageSize > 0 && pageSize <= maxPagSize) ? pageSize : maxPagSize;

            int skip = (pageNumber - 1) * pageSize;
            int take = pageSize;

            string query = @"SELECT 
                            COUNT(*)
                            FROM Notification
 
                            SELECT  * FROM Notification
                            ORDER BY Id
                            OFFSET @Skip ROWS FETCH NEXT @Take ROWS ONLY";

            using (SqlConnection _dapper = new SqlConnection(_connectionString))
            {
                var reader = _dapper.QueryMultiple(query, new { Skip = skip, Take = take });

                int count = reader.Read<int>().FirstOrDefault();
                List<Notification> AllTransactions = reader.Read<Notification>().ToList();

                //var result = new PagingResponseModel<List<Notification>>(AllTransactions, count, currentPageNumber, pageSize);

                dynamic validFilter = new PaginationFilter(pageNumber, pageSize);
                var route = Request.Path.Value;
                //return result;

                var pagedReponse = PaginationHelper.CreatePagedReponse<Notification>(AllTransactions, validFilter, count, _uriService, route);

                PagedResponse<List<Notification>> response = pagedReponse;
                //return ;

                return Ok(response);
            }
        }

        [HttpGet("GetAlternativePagedData")]
        public async Task<IActionResult> GetPaginatedTransactionList([FromQuery] PaginationFilter filter)
        {
            Console.WriteLine("--- Getting Paginated Transaction List ---");
            try
            {
                var route = Request.Path.Value;
                var bankIdRequest = Request.Query["bankId"];

                string userGroupId = Request.Query["groupId"];
                string userAccountType = Request.Query["accType"];

                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var pagedReponse = await _repository.GetPaginatedTransactions(validFilter, route, bankIdRequest, Convert.ToInt32(userGroupId), userAccountType);

                pagedReponse.Succeeded = true;
                pagedReponse.Message = "Successful";
                pagedReponse.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                pagedReponse.Errors = null;

                return Ok(pagedReponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetPaginatedTransactionList ==> {ex.Message}");

                var Error = new[] { ex.Message };
                return Ok(new Response<PagedResponse>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpGet("TransactionsFilter")]
        public async Task<IActionResult> TransactionsFilter([FromQuery] TransactionFilterDto filterRequest, [FromQuery] PaginationFilter filter)
        {
            Console.WriteLine("--- Getting Filtered Transactions ---");
            try
            {
                var route = Request.Path.Value;
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

                string bankIdRequest = Request.Query["bankId"];

                string userGroupId = Request.Query["groupId"];
                string userAccountType = Request.Query["accType"];

                var filteredTransactions = await _repository.PosTransactionsFilter(filterRequest, validFilter, route, bankIdRequest, Convert.ToInt32(userGroupId), userAccountType);

                filteredTransactions.Succeeded = true;
                filteredTransactions.Message = ApplicationConstant.SuccessMessage;
                filteredTransactions.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                filteredTransactions.Errors = null;

                return Ok(filteredTransactions);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : TransactionsFilter ==> {ex.Message}");
                var Error = new[] { ex.Message };
                return Ok(new Response<TransactionReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpGet("GetDashboardTransactions")]
        public async Task<ActionResult> GetDashboardTransactions()
        {
            Console.WriteLine("--- Getting Dashboard Transactions ---");
            try
            {
                string bankIdRequest = Request.Query["bankId"];
                string userGroupId = Request.Query["groupId"];
                string userAccountType = Request.Query["accType"];

                //Console.WriteLine("--- Bank ID Received ---"+ bankIdRequest);
                var dashboardTransactions = await _repository.GetTransactionsAppDashboardTransactions(bankIdRequest, Convert.ToInt32(userGroupId), userAccountType);

                return Ok(new Response<DashboardDetailsDto>
                {
                    Data = dashboardTransactions,
                    Succeeded = true,
                    Message = ApplicationConstant.SuccessMessage,
                    Errors = null
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetDashboardTransactions ==> {ex.Message}");

                var Error = new[] { ex.Message };
                return Ok(new Response<DashboardDetailsDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpGet("DashboardChart")]
        public async Task<ActionResult> GetChartData()
        {
            Console.WriteLine("--- Getting Chart Data ---");
            try
            {
                string bankIdRequest = Request.Query["bankId"];
                string userGroupId = Request.Query["groupId"];
                string userAccountType = Request.Query["accType"];

                var dashboardTransactions = await _repository.GetChartData(bankIdRequest, Convert.ToInt32(userGroupId), userAccountType);

                return Ok(new Response<ChartTransactionsRead>
                {
                    Data = dashboardTransactions,
                    Succeeded = true,
                    Message = ApplicationConstant.SuccessMessage,
                    Errors = null
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetChartData ==> {ex.Message}");

                var Error = new[] { ex.Message };
                return Ok(new Response<ChartTransactionsRead>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpGet("GetApprovedTransactions")]
        public async Task<ActionResult> GetApprovedTransactions([FromQuery] PaginationFilter filter)
        {
            Console.WriteLine("--- Getting Filtered Transactions ---");
            try
            {
                var route = Request.Path.Value;
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

                string bankIdRequest = Request.Query["bankId"];
                string userGroupId = Request.Query["groupId"];
                string userAccountType = Request.Query["accType"];

                var approvedTransactions = await _repository.GetApprovedTransactions(filter, route, bankIdRequest, Convert.ToInt32(userGroupId), userAccountType);

                approvedTransactions.Succeeded = true;
                approvedTransactions.Message = ApplicationConstant.SuccessMessage;
                approvedTransactions.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                approvedTransactions.Errors = null;

                return Ok(approvedTransactions);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetApprovedTransactions ==> {ex.Message}");

                var Error = new[] { ex.Message };
                return Ok(new Response<TransactionReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpGet("GetDeclinedTransactions")]
        public async Task<ActionResult> GetDeclinedTransactions([FromQuery] PaginationFilter filter)
        {
            Console.WriteLine("--- Getting Filtered Transactions ---");
            

            try
            {
                string bankIdRequest = Request.Query["bankId"];
                string userGroupId = Request.Query["groupId"];
                string userAccountType = Request.Query["accType"];

                var route = Request.Path.Value;
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);

                var declinedTransactions = await _repository.GetDeclinedTransactions(filter, route, bankIdRequest, Convert.ToInt32(userGroupId), userAccountType);

                declinedTransactions.Succeeded = true;
                declinedTransactions.Message = ApplicationConstant.SuccessMessage;
                declinedTransactions.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                declinedTransactions.Errors = null;

                return Ok(declinedTransactions);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetDeclinedTransactions ==> {ex.Message}");
                var Error = new[] { ex.Message };
                return Ok(new Response<TransactionReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }

        [HttpGet("AllBanks")]
        public async Task<IActionResult> GetBankLists()
        {
            var apiReturnResponse = new ApiResponse();

            try
            {
                var bankList = await _bankRepo.GetPaginatedBanksList();

                apiReturnResponse.ResponseCode = "00";
                apiReturnResponse.ResponseMessage = "Successfully retrieved banks list";
                apiReturnResponse.Data = bankList;
                return Ok(apiReturnResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetBankLists ==> {ex.Message}");
                var Error = new[] { ex.Message };
                return Ok(new Response<TransactionReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }
        }

        [HttpGet("GetBankWithPrefix")]
        public async Task<IActionResult> GetBankAndPrefix()
        {
            var apiReturnResponse = new ApiResponse();

            try
            {
                var bankAndPrefix = await _bankRepo.GetBankWithPrefixList();

                apiReturnResponse.ResponseCode = "00";
                apiReturnResponse.ResponseMessage = "Successfully retrieved banks list";
                apiReturnResponse.Data = bankAndPrefix;
                return Ok(apiReturnResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetBankAndPrefix ==> {ex.Message}");
                var Error = new[] { ex.Message };
                return Ok(new Response<TransactionReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }
        }

        [HttpPost("CreateNewBankProfile")]
        //[Authorize]
        public async Task<IActionResult> CreateBankProfile([FromBody] BankCreateDto JsonPayload)
        {
            var apiReturnResponse = new ApiResponse();
            string createdbyUserId = User.Claims.ToList()[3].Value;

            //var JsonObject = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(JsonPayload);

            Console.WriteLine("--- Payload received ---" + JsonPayload.BankName);


           // return (IActionResult)JsonObject;

            try
            {
                //Console.WriteLine("--- Bank Name received ---"+JsonObject["BankName"]);

                var bankAlreadyExists = await _bankRepo.FindBankByName(JsonPayload.BankName.ToString());

                Console.WriteLine("---Checked existing Name ---" + bankAlreadyExists);

                if (null != bankAlreadyExists)
                {
                    apiReturnResponse.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    apiReturnResponse.ResponseMessage = "Bank with same name already exists";
                    return Ok(apiReturnResponse);
                }


                dynamic resp = await _bankRepo.CreateBanksList(JsonPayload, Convert.ToInt32(createdbyUserId));

                Console.WriteLine("--- Created Bank ---" + resp);

                


                if (resp.Id > 0)
                {


                    foreach (dynamic prefix in JsonPayload.Prefixes)
                    {
                        Console.WriteLine("--- Bank Prefix to be added ---" +prefix);

                        if (null == prefix || prefix == "")
                            continue;
                        await _prefixRepo.CreateTerminalPrefix(prefix, Convert.ToInt32(resp.Id));
                    }

                    apiReturnResponse.ResponseCode = "00";
                    apiReturnResponse.ResponseMessage = "New bank added successfully";
                    return Ok(apiReturnResponse);
                }
                else
                {

                    apiReturnResponse.ResponseCode = "06";
                    apiReturnResponse.ResponseMessage = "Could not create new bank record";
                    return NotFound(apiReturnResponse);

                }

                


            }
            catch(Exception e)
            {
                _logger.LogError($"Exception Occured While creating new bank profile ==> {e.Message}");
                var Error = new[] { e.Message };
                return Ok(new Response<BankReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }
        }

    }
}
