﻿using System;
namespace PosMonitoring.Dtos
{
    public class MerchantGroupsReadDto
    {
        public int GrpIrn { get; set; }
        public string GroupName { get; set; }
        public string MerchantId { get; set; }
        public string MerchantName { get; set; }
        public string Desdcription { get; set; }
        public int GroupStatus { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public class MerchantGroupsCreateResponse
    {
        public int Id { get; set; }
    }


    public class RoleCreateDto
    {
        public string RoleName { get; set; }
    }

    public class RoleReadDto
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleCreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
    }


    public class RoleCreateResponse
    {
        public int RoleCreated { get; set; }
    }
}
