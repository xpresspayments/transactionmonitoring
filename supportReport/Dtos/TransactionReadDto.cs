﻿using System;
namespace PosMonitoring.Dtos
{
    public class TransactionReadDto
    {
        public int id { get; set; }
        public string merchantId { get; set; }
        public string mti { get; set; }
        public string processingcode { get; set; }
        public string amount { get; set; }
        public string stan { get; set; }
        public string pan { get; set; }
        public string track2 { get; set; }
        public string track1 { get; set; }
        public string iccdata { get; set; }
        public string posentrymode { get; set; }
        public string refcode { get; set; }

        public string posconditioncode { get; set; }
        public string aurhorisationresponse { get; set; }
        public string currencycode { get; set; }
        public string terminalid { get; set; }
        public string transactiondate { get; set; }
        public string transactiontime { get; set; }

        public string systemtime { get; set; }
        public string responcecode { get; set; }
        public string trantype { get; set; }
        public string batchno { get; set; }
        public string seqno { get; set; }
        public string t_status { get; set; }
        public string responsedescription { get; set; }
        public string remote_server_ip { get; set; }

        public string firmware_id { get; set; }
        public string timelocaltransaction { get; set; }
        public string datelocaltransaction { get; set; }
        public string rrn { get; set; }
        public string trancode { get; set; }
        public string searchtext { get; set; }
        public string accounttype { get; set; }
        public string cellinfo { get; set; }

        public string gpsinfo { get; set; }
        public string otherterminalid { get; set; }
        public string channelCode { get; set; }
        public string paymentMethod { get; set; }
        public string customerName { get; set; }
        public string revenueCode { get; set; }
        public string narration { get; set; }
        public string customerOtherInfo { get; set; }

        public int tmsPushed { get; set; }
        public int reportPushed { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime tmsPushedDate { get; set; }
        public DateTime reportPushedDate { get; set; }
        public int paxTransaction { get; set; }

        public string merchantName { get; set; }
        public string merchantAddress { get; set; }
        public string footer { get; set; }
        public string transTypeCode { get; set; }
        public string authorizationCode { get; set; }

        public string TerminalIdPrefix { get; set; }
        public string groupTid { get; set; }

    }

    public class TransactionReadDtoVm
    {
        public int id { get; set; }
        public string merchantId { get; set; }
        public string mti { get; set; }
        public string processingcode { get; set; }
        public string amount { get; set; }
        public string stan { get; set; }
        public string pan { get; set; }
        public string track2 { get; set; }
        public string track1 { get; set; }
        public string iccdata { get; set; }
        public string posentrymode { get; set; }
        public string refcode { get; set; }

        public string posconditioncode { get; set; }
        public string aurhorisationresponse { get; set; }
        public string currencycode { get; set; }
        public string terminalid { get; set; }
        public string transactiondate { get; set; }
        public string transactiontime { get; set; }

        public string systemtime { get; set; }
        public string responcecode { get; set; }
        public string trantype { get; set; }
        public string batchno { get; set; }
        public string seqno { get; set; }
        public string t_status { get; set; }
        public string responsedescription { get; set; }
        public string remote_server_ip { get; set; }

        public string firmware_id { get; set; }
        public string timelocaltransaction { get; set; }
        public string datelocaltransaction { get; set; }
        public string rrn { get; set; }
        public string trancode { get; set; }
        public string searchtext { get; set; }
        public string accounttype { get; set; }
        public string cellinfo { get; set; }
        public string gpsinfo { get; set; }
        public string otherterminalid { get; set; }
        public string channelCode { get; set; }
        public string paymentMethod { get; set; }
        public string customerName { get; set; }
        public string revenueCode { get; set; }
        public string narration { get; set; }
        public string customerOtherInfo { get; set; }

        public int tmsPushed { get; set; }
        public int reportPushed { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime tmsPushedDate { get; set; }
        public DateTime reportPushedDate { get; set; }
        public int paxTransaction { get; set; }

        public string merchantName { get; set; }
        public string merchantAddress { get; set; }
        public string footer { get; set; }
        public string transTypeCode { get; set; }
        public string authorizationCode { get; set; }

        public string TerminalIdPrefix { get; set; }
        public string groupTid { get; set; }
    }
}
