﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Dtos
{
    public class DashboardDetailsDto
    {
        public int AllTransCount { get; set; }
        public Decimal AllTransAmount { get; set; }
        public int ApprovedTransCount { get; set; }
        public Decimal ApprovedTransAmount { get; set; }
        public int DeclinedTransCount { get; set; }
        public Decimal DeclinedTransAmount { get; set; }
        public int groupTerminal { get; set; }
    }
}
