﻿using System;
using System.Collections.Generic;

namespace PosMonitoring.Dtos
{
    public class BankCreateDto
    {
        public string BankName { get; set; }
        public List<string> Prefixes { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

}
