﻿using System;
namespace PosMonitoring.Dtos
{
    public class GroupAsignUserCreateDto
    {
        public string UserEmail { get; set; }
        public int GroupId { get; set; }
    }


    public class UserAssignToGroupResponse
    {
        public int UserId { get; set; }
    }
}
