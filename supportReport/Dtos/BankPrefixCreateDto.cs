﻿using System;
namespace PosMonitoring.Dtos
{
    public class BankPrefixCreateDto
    {
        public int Id {get; set;}
        public int BankId { get; set; }
        public string TerminalIdPrefix { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class BankPrefixResponse
    {
        public long Id { get; set; }
    }
}
