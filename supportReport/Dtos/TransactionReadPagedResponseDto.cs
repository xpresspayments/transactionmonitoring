﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Dtos
{
  
    public class Datum
    {
       /* public int id { get; set; }
        public string account_type { get; set; }
        public string amount { get; set; }
        public string authorization_code { get; set; }
        public object authorization_response { get; set; }
        public string batch_no { get; set; }
        public string cell_info { get; set; }
        public string channel_code { get; set; }
        public string created_date { get; set; }
        public string currency_code { get; set; }
        public string customer_name { get; set; }
        public string customer_other_info { get; set; }
        public string date_local_transaction { get; set; }
        public string echo_data { get; set; }
        public string firmware_id { get; set; }
        public string footer { get; set; }
        public object gps_info { get; set; }
        public string icc_data { get; set; }
        public string merchant_address { get; set; }
        public string merchant_id { get; set; }
        public string merchant_name { get; set; }
        public string mti { get; set; }
        public object narration { get; set; }
        public string other_terminal_id { get; set; }
        public string pan { get; set; }
        public object pax_transaction { get; set; }
        public object payment_information { get; set; }
        public string payment_method { get; set; }
        public string pos_condition_code { get; set; }
        public string pos_entry_mode { get; set; }
        public object processing_code { get; set; }
        public string ref_code { get; set; }
        public string remote_server_ip { get; set; }
        public object report_pushed { get; set; }
        public object report_pushed_date { get; set; }
        public string response_code { get; set; }
        public string response_description { get; set; }
        public object revenue_code { get; set; }
        public string rrn { get; set; }
        public object search_text { get; set; }
        public string seq_no { get; set; }
        public string stan { get; set; }
        public string system_time { get; set; }
        public string terminal_id { get; set; }
        public string time_local_transaction { get; set; }
        public object tms_pushed { get; set; }
        public object tms_pushed_date { get; set; }
        public object track1 { get; set; }
        public string track2 { get; set; }
        public string tran_type { get; set; }
        public string trans_type_code { get; set; }
        public object transaction_code { get; set; }
        public string transaction_date { get; set; }
        public string transaction_id { get; set; }
        public string transaction_status { get; set; }
        public string transaction_time { get; set; }
        */
        public int id { get; set; }
        public string merchantId { get; set; }
        public string mti { get; set; }
        public string processingcode { get; set; }
        public string amount { get; set; }
        public string stan { get; set; }
        public string pan { get; set; }
        public string track2 { get; set; }
        public string track1 { get; set; }
        public string iccdata { get; set; }
        public string posentrymode { get; set; }
        public string refcode { get; set; }
        public string posconditioncode { get; set; }
        public string aurhorisationresponse { get; set; }
        public string currencycode { get; set; }
        public string terminalid { get; set; }
        public string transactiondate { get; set; }
        public string transactiontime { get; set; }
        public string systemtime { get; set; }
        public string responcecode { get; set; }
        public string trantype { get; set; }
        public string batchno { get; set; }
        public string seqno { get; set; }
        public string t_status { get; set; }
        public string responsedescription { get; set; }
        public string remote_server_ip { get; set; }
        public string firmware_id { get; set; }
        public string timelocaltransaction { get; set; }
        public string datelocaltransaction { get; set; }
        public string rrn { get; set; }
        public string trancode { get; set; }
        public string searchtext { get; set; }
        public string accounttype { get; set; }
        public string cellinfo { get; set; }
        public string gpsinfo { get; set; }
        public string otherterminalid { get; set; }
        public string channelCode { get; set; }
        public string paymentMethod { get; set; }
        public string customerName { get; set; }
        public string revenueCode { get; set; }
        public string narration { get; set; }
        public string customerOtherInfo { get; set; }
        public string tmsPushed { get; set; }
        public string reportPushed { get; set; }
        public string createdDate { get; set; }
        public string tmsPushedDate { get; set; }
        public string reportPushedDate { get; set; }
        public string paxTransaction { get; set; }
        public string merchantName { get; set; }
        public string merchantAddress { get; set; }
        public string footer { get; set; }
        public string transTypeCode { get; set; }
        public string authorizationCode { get; set; }
    }

    public class TransactionReadPagedResponseDto
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string FirstPage { get; set; }
        public string LastPage { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        public object NextPage { get; set; }
        public object PreviousPage { get; set; }
        public List<Datum> Data { get; set; }
        public bool Succeeded { get; set; }
        public object Errors { get; set; }
        public object Message { get; set; }
    }


}
