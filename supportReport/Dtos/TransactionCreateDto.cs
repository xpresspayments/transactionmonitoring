﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PosMonitoring.Dtos
{
    public class TransactionCreateDto
    {
        [Required]
        public string account_type { get; set; }

        [Required]
        public string amount { get; set; }

        [Required]
        public string authorization_code { get; set; }

        [Required]
        public string authorization_response { get; set; }

        [Required]
        public string batch_no { get; set; }
        public string cell_info { get; set; }
        public string channel_code { get; set; }
        public string created_date { get; set; }
        public string currency_code { get; set; }
        public string customer_name { get; set; }
        public string customer_other_info { get; set; }
        public string date_local_transaction { get; set; }
        public string echo_data { get; set; }
        public string firmware_id { get; set; }
        public string footer { get; set; }
        public string gps_info { get; set; }
        public string icc_data { get; set; }
        public string merchant_address { get; set; }
        public string merchant_id { get; set; }
        public string merchant_name { get; set; }
        public string mti { get; set; }
        public string narration { get; set; }
        public string other_terminal_id { get; set; }
        public string pan { get; set; }
        public string pax_transaction { get; set; }
        public string payment_information { get; set; }
        public string payment_method { get; set; }
        public string pos_condition_code { get; set; }
        public string pos_entry_mode { get; set; }
        public string processing_code { get; set; }
        public string ref_code { get; set; }
        public string remote_server_ip { get; set; }
        public string report_pushed { get; set; }
        public string report_pushed_date { get; set; }
        public string response_code { get; set; }
        public string response_description { get; set; }
        public string revenue_code { get; set; }

        [Required]
        public string rrn { get; set; }
        public string search_text { get; set; }
        public string seq_no { get; set; }
        public string stan { get; set; }
        public string system_time { get; set; }

        [Required]
        public string terminal_id { get; set; }
        public string time_local_transaction { get; set; }
        public string tms_pushed { get; set; }
        public string tms_pushed_date { get; set; }
        public string track1 { get; set; }

        [Required]
        public string track2 { get; set; }
        public string tran_type { get; set; }
        public string trans_type_code { get; set; }
        public string transaction_code { get; set; }
        public string transaction_date { get; set; }
        public string transaction_id { get; set; }
        public string transaction_status { get; set; }
        public string transaction_time { get; set; }
    }
}
