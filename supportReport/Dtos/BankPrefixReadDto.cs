﻿using System;
namespace PosMonitoring.Dtos
{
    public class BankPrefixReadDto
    {
        public int Id { get; set; }
        public int BankId { get; set; }
        public string TerminalIdPrefix { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
