﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PosMonitoring.Dtos.Account
{
    public class DeactivateUserDto
    {
        [Required]
        public string Email { get; set; }

        public string DeactivatedComment { get; set; }
        [JsonIgnore]
        public bool IsActive { get; set; }
    }
}
