﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Dtos.Account
{
    public class CreateUserDto
    {
        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be {2} characters long.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be {2} characters long.")]
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        public int RoleId { get; set; }

        public long UserBankId { get; set; }

        public string UserAccountType { get; set; }

        public int UserGroupId { get; set; }
    }

    public class CreateUserResponse
    {
        public string DefaultPassword { get; set; }
        public dynamic IsCreated { get; set; }
    }
}
