﻿using System;
namespace PosMonitoring.Dtos
{
    public class MerchantGroupsCreateDto
    {
        public string GroupName { get; set; }
        public string MerchantId { get; set; }
        public string MerchantName { get; set; }
        public string Desdcription { get; set; }
    }
}
