﻿using System;
namespace PosMonitoring.Dtos
{
    public class TerminalsCreateDto
    {
        public int TrmGrpIrn { get; set; }
        public string TrmTerminalId { get; set; }
    }
}
