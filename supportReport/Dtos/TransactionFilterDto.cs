﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Dtos
{
    public class TransactionFilterDto
    {
        public string TransactionId { get; set; }
        public string MerchantId { get; set; }
        public string TerminalId { get; set; }
        public string TransactionStatus { get; set; }
        public string CustomerName { get; set; }
        public string RRN { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class TransactionFilterFormatterDto
    {
        public string TransactionId { get; set; }
        public string MerchantId { get; set; }
        public string TerminalId { get; set; }
        public string TransactionStatus { get; set; }
        public string CustomerName { get; set; }
        public string RRN { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}
