﻿using System.Collections.Generic;

namespace PosMonitoring.AppConstants
{
    public class ApplicationConstant
    {
        public static string SuccessResponseCode = "00";
        public static int successResponseCode = 00;
        public static string FailureResponse = "-1";
        public static int SuccessStatusCode = 200;
        public static int NotFoundStatusCode = 404;
        public static int NotAuthenticatedStatusCode = 401;
        public static int BadRequestStatusCode = 400;
        public static int ExceptionOccuredStatusCode = -1;
        public static string SuccessMessage = "Successful";
        public static string FailureMessage = "Failed";


        //StoredProcedures
        public static string Sp_PosTransactionReport = "Sp_PosTransactionReport";
        public static string Sp_DashboardTransactionReport = "Sp_DashboardTransactionReport";
        public static string Sp_UserAuthandLogin = "Sp_UserAuthandLogin";

        public static string Sp_BanksDetails = "Sp_BanksDetails";

        public static string Sp_BankDeclinedAndApproved = "Sp_BankDeclinedAndApproved";

        public static string Sp_MerchantGroup = "Sp_MerchantGroup";

        public static string Sp_RolesManagement = "Sp_RolesManagement";
    }
}
