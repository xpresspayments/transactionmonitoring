﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PosMonitoring.AppConstants;
using PosMonitoring.Data;
using PosMonitoring.Data.UserAccount.IRepository;
using PosMonitoring.Dtos;
using PosMonitoring.Enums;
using PosMonitoring.GenericResponse;
using PosMonitoring.Models;
using PosMonitoring.SyncDataServices.Http;
using PosMonitoring.Wrappers;

namespace PosMonitoring.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MerchantGroupsController: ControllerBase
    {
        private readonly IMerchantGroups _repository;
        private readonly IMapper _mapper;
        private readonly ICommandDataClient _commandDataClient;
        private readonly ILogger<TransactionsController> _logger;
        private readonly IBankRepo _bankRepo;
        private readonly ITerminalsRepo _terminalRepo;
        private readonly IAccountRepository _userAccountRepo;

        public MerchantGroupsController(
            IMerchantGroups repository,
            ILogger<TransactionsController> logger,
            IMapper mapper,
            ICommandDataClient commandDataClient,
            IBankRepo bankRepo,
            ITerminalsRepo terminal,
            IAccountRepository userAccount
            )
        {
            _repository = repository;
            _mapper = mapper;
            _commandDataClient = commandDataClient;
            _logger = logger;
            _bankRepo = bankRepo;
            _terminalRepo = terminal;
            _userAccountRepo = userAccount;
        }

        //GET ALL GROUPS
        [HttpGet("MerchantGroupsList")]
        [Authorize]
        public async Task<ActionResult> MerchantGroupsList([FromQuery] PaginationFilter filter)
        {
            Console.WriteLine("---Getting list of groups---");
            try
            {
                var route = Request.Path.Value;
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var pagedReponse = await _repository.GetAllGroups(validFilter, route);

                pagedReponse.Succeeded = true;
                pagedReponse.Message = "Successful";
                pagedReponse.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                pagedReponse.Errors = null;

                Console.WriteLine(pagedReponse);

                return Ok(pagedReponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : MerchantGroupsList ==> {ex.Message}");

                var Error = new[] { ex.Message };
                return Ok(new Response<PagedResponse>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }


        //GET GROUPS BY MERCHANT ID
        [HttpGet("GetGroupByMerchant")]
        [Authorize]
        public async Task<ActionResult> GetGroupByMerchant([FromQuery] PaginationFilter filter)
        {
            Console.WriteLine("---Getting list of groups---");
            try
            {
                var route = Request.Path.Value;
                string merchantId = Request.Query["mid"];
                var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
                var pagedReponse = await _repository.GetGroupsByMerchant(validFilter, route, merchantId);

                pagedReponse.Succeeded = true;
                pagedReponse.Message = "Successful";
                pagedReponse.ResponseCode = ResponseCode.Ok.ToString("D").PadLeft(2, '0');
                pagedReponse.Errors = null;

                return Ok(pagedReponse);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception Occured: ControllerMethod : GetGroupByMerchant ==> {ex.Message}");

                var Error = new[] { ex.Message };
                return Ok(new Response<PagedResponse>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }

        }


        //GET A SINGLE GROUP
        [HttpGet("GetGroupById/{id}", Name = "GetGroupById")]
        public async Task<ActionResult<MerchantGroupsReadDto>> GetGroupById(string id)
        {
            var getTxnResult = await _repository.GetGroupById(id);

            if (null != getTxnResult)
            {
                return Ok(_mapper.Map<MerchantGroupsReadDto>(getTxnResult));
            }

            return NotFound();

        }


        //CREATE A NEW GROUP
        [HttpPost("CreateNewMerchantGroup")]
        public async Task<IActionResult> CreateBankProfile([FromBody] MerchantGroupsCreateDto groupData)
        {
            var apiReturnResponse = new ApiResponse();
            string createdbyUserId = User.Claims.ToList()[3].Value;
            

            try
            {

                //FIND IF MERCHANT'S GROUP ALREADY EXISTS
                var merchantGroupExists = await _repository.FindGroupByMerchantAndGroupName(groupData.MerchantId, groupData.GroupName);

                if (null != merchantGroupExists)
                {
                    apiReturnResponse.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    apiReturnResponse.ResponseMessage = "There is an existing group with same name for this merchant";
                    return Ok(apiReturnResponse);
                }


                dynamic resp = await _repository.CreateNewMerchantGroup(groupData, createdbyUserId);

                Console.WriteLine("--- Group Created ---" + resp);




                if (resp.Id > 0)
                {

                    apiReturnResponse.ResponseCode = "00";
                    apiReturnResponse.ResponseMessage = "Successfully created a new group for "+groupData.GroupName;
                    return Ok(apiReturnResponse);
                }
                else
                {

                    apiReturnResponse.ResponseCode = "06";
                    apiReturnResponse.ResponseMessage = "Could not create new group for merchant";
                    return Ok(apiReturnResponse);

                }




            }
            catch (Exception e)
            {
                _logger.LogError($"Exception Occured While creating new merchant group ==> {e.Message}");
                var Error = new[] { e.Message };
                return Ok(new Response<MerchantGroupsReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }
        }


        //ASSIGN TERMINAL TO GROUP
        [HttpPost("AssinNewTidToGroup")]
        public async Task<ActionResult> AssinNewTidToGroup([FromBody] TerminalsCreateDto trm)
        {
            var apiReturnResponse = new ApiResponse();
            string createdbyUserId = User.Claims.ToList()[3].Value;

            try
            {

                var checkIfTerminalExists = await _terminalRepo.FindByTerminalId(trm.TrmTerminalId, trm.TrmGrpIrn);

                if (null != checkIfTerminalExists)
                {
                    apiReturnResponse.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    apiReturnResponse.ResponseMessage = "This TID is already assigned to this group";
                    return Ok(apiReturnResponse);
                }

                dynamic resp = await _terminalRepo.AddTerminalToGroup(trm, createdbyUserId);

                if (resp.Id > 0)
                {

                    apiReturnResponse.ResponseCode = "00";
                    apiReturnResponse.ResponseMessage = "Successfully added terminal to group ";
                    return Ok(apiReturnResponse);
                }
                else
                {

                    apiReturnResponse.ResponseCode = "06";
                    apiReturnResponse.ResponseMessage = "Unable to add terminal to group";
                    return Ok(apiReturnResponse);

                }


            }
            catch (Exception e)
            {
                _logger.LogError($"Exception Occured While assigning terminal to group ==> {e.Message}");
                var Error = new[] { e.Message };
                return Ok(new Response<MerchantGroupsReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }
        }


        //ASSIGN BANK USER TO GROUP
        [HttpPost("AssignUserToMerchantGroup")]
        public async Task<ActionResult> AssignUserToMerchantGroup([FromBody] GroupAsignUserCreateDto userData)
        {
            var apiReturnResponse = new ApiResponse();
            string createdbyUserId = User.Claims.ToList()[3].Value;

            try
            {

                var getUserDetails = await _userAccountRepo.FindUser(userData.UserEmail);


                Console.WriteLine("THE SELECTED USER -- " + getUserDetails.UserId);
                if (null != getUserDetails)
                {

                    if(getUserDetails.RoleId != 46)
                    {
                        apiReturnResponse.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                        apiReturnResponse.ResponseMessage = "Only merchant users profile can be asigned to groups";
                        return Ok(apiReturnResponse);

                    }


                    //CONTINUE UPDATE
                    dynamic assignUsrResp = await _terminalRepo.AssignUserToGroup(userData, createdbyUserId, getUserDetails.UserId.ToString());

                    if (assignUsrResp.UserId > 0)
                    {

                        apiReturnResponse.ResponseCode = "00";
                        apiReturnResponse.ResponseMessage = "User "+userData.UserEmail+" Has been assign to merchant group successfully";
                        return Ok(apiReturnResponse);
                    }
                    else
                    {

                        apiReturnResponse.ResponseCode = "06";
                        apiReturnResponse.ResponseMessage = "Unable to assign user to group";
                        return Ok(apiReturnResponse);

                    }

                }
                else
                {
                    apiReturnResponse.ResponseCode = ResponseCode.DuplicateError.ToString("D").PadLeft(2, '0');
                    apiReturnResponse.ResponseMessage = "No user account found with provided email";
                    return Ok(apiReturnResponse);
                }


            }
            catch (Exception e)
            {
                _logger.LogError($"Exception Occured While assigning terminal to group ==> {e.Message}");
                var Error = new[] { e.Message };
                return Ok(new Response<MerchantGroupsReadDto>
                {
                    Data = null,
                    Succeeded = false,
                    Message = ApplicationConstant.FailureMessage,
                    Errors = Error
                });
            }
        }

    }
}
