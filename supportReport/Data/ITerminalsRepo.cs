﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PosMonitoring.Dtos;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;
using PosMonitoring.Dtos.Account;
namespace PosMonitoring.Data
{
    public interface ITerminalsRepo
    {
        Task<TerminalsCreateResponse> AddTerminalToGroup(TerminalsCreateDto termsData, string addedBy);
        Task<PagedResponse<List<Terminals>>> GetAllTerminals(PaginationFilter validFilter, string route);
        Task<Terminals> FindByTerminalId(string tid, int groupId);
        Task<UserAssignToGroupResponse> AssignUserToGroup(GroupAsignUserCreateDto AsignToGroup, string asignBy, string userId);
    }
}
