﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PosMonitoring.Dtos;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;
namespace PosMonitoring.Data
{
    public interface IMerchantGroups
    {
        Task<PagedResponse<List<MerchantGroups>>> GetAllGroups(PaginationFilter validFilter, string route);
        Task<PagedResponse<List<MerchantGroups>>> GetGroupsByMerchant(PaginationFilter validFilter, string route, string MerchantId);
        Task<MerchantGroupsCreateResponse> CreateNewMerchantGroup(MerchantGroupsCreateDto groupData, string CreatedBy);
        Task<MerchantGroups> GetGroupById(string GroupId);
        Task<MerchantGroups> FindGroupByMerchantAndGroupName(string merchantId, string groupName);
    }
}
