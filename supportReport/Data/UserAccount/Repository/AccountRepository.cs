﻿using Dapper;
using MailKit.Net.Smtp;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;
using PosMonitoring.AppConstants;
using PosMonitoring.Data.UserAccount.IRepository;
using PosMonitoring.Dtos.Account;
using PosMonitoring.Enums;
using PosMonitoring.GenericResponse;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace PosMonitoring.Data.UserAccount.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private string _connectionString;
        private readonly ILogger<AccountRepository> _logger;
        private readonly IConfiguration _configuration;
        private readonly IBankRepo _bankRepo;

        public AccountRepository(IConfiguration configuration, ILogger<AccountRepository> logger, IBankRepo bankRepo)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _logger = logger;
            _configuration = configuration;
            _bankRepo = bankRepo;
        }

        public async Task<User> FindUser(string email)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.FETCH);
                    param.Add("@Email", email);

                    var userDetails = await _dapper.QueryFirstOrDefaultAsync<User>(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return userDetails;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: FindUser(string email) ===>{ ex.Message}");
                throw;
            }
        }
        
        public async Task<CreateUserResponse> AddUser(CreateUserDto user, int createdbyUserId)
        {
            try
            {
                var response = new CreateUserResponse();
                var defaultPassword = Utils.RandomPassword();
                var passwordHash = BCrypt.Net.BCrypt.HashPassword(defaultPassword, BCrypt.Net.BCrypt.GenerateSalt());

                string userAccountType = string.Empty;
                var userRoleId = Convert.ToInt32(user.RoleId);

                userAccountType = userRoleId switch
                {
                    1 => "ADMIN_USER",
                    2 => "XPRESS_ADMIN",
                    3 => "MERCHANT_ADMIN",
                    44 => "BANK_USER",
                    46 => "MERCHANT_USER",
                    _ => null,
                };


                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.ADDUSER);
                    param.Add("@FirstName", user.FirstName == null ? "" : user.FirstName.ToString());
                    param.Add("@MiddleName", user.MiddleName == null ? "" : user.MiddleName.ToString());
                    param.Add("@LastName", user.LastName == null ? "" : user.LastName.ToString());
                    param.Add("@UserEmail", user.Email == null ? "" : user.Email.ToString().Trim());
                    param.Add("@PhoneNumber", user.PhoneNumber == null ? "" : user.PhoneNumber.ToString().Trim());
                    param.Add("@PasswordHash", passwordHash);
                    param.Add("@RoleId", user.RoleId);
                    param.Add("@CreatedByUserId", createdbyUserId);
                    param.Add("@UserBankId", user.UserBankId == 0 ? "" : user.UserBankId);
                    param.Add("@UserAccountType", userAccountType);




                    dynamic isCreated = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);


                    //Console.WriteLine("NEW ACCOUNT CREATION RESPONSE --- " + isCreated);

                    response.IsCreated = isCreated;
                    response.DefaultPassword = defaultPassword;
                    return response;
                   
                    
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"MethodName: AddUser(CreateUserDto user) ===>{ex.Message}");
                throw;
            }
        }

        public async Task<dynamic> UpdateUser(UpdateUserDto user, int updatedbyUserId)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.UPDUSER);
                    param.Add("@UserToupdate", user.Email);
                    param.Add("@FirstNameUpdate", user.FirstName == null ? "" : user.FirstName.ToString());
                    param.Add("@MiddleNameUpdate", user.MiddleName == null ? "" : user.MiddleName.ToString());
                    param.Add("@LastNameUpdate", user.LastName == null ? "" : user.LastName.ToString());
                    param.Add("@PhoneNumberUpdate", user.PhoneNumber == null ? "" : user.PhoneNumber.ToString().Trim());
                    param.Add("@RoleIdUpdate", user.RoleId);
                    param.Add("@UpdatedByUserId", updatedbyUserId);

                    dynamic response = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);
                    
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: UpdateUser(UpdateUserDto user, int updatedbyUserId) ===>{ex.Message}");
                throw;
            }
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.GETALLUSERS);

                    var userDetails = await _dapper.QueryAsync<User>(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return userDetails;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetAllUsers() ===>{ ex.Message}");
                throw;
            }
        }

        public async Task<IEnumerable<User>> GetUsersPendingApproval()
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.USERSPENDINGAPPROVAL);

                    var userDetails = await _dapper.QueryAsync<User>(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return userDetails;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetUsersPendingApproval() ===>{ ex.Message}");
                throw;
            }
        }

        public async Task<dynamic> ApproveUser(long approvedByuserId, string defaultPass, string userEmail)
        {
            try
            {
                var hashdefaultPassword = BCrypt.Net.BCrypt.HashPassword(defaultPass, BCrypt.Net.BCrypt.GenerateSalt());

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.APPROVEUSER);
                    param.Add("@PasswordHashApprove", hashdefaultPassword);
                    param.Add("@UserIdApprove", approvedByuserId);
                    param.Add("@UserEmailApprove", userEmail);

                    dynamic resp = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return resp;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: ApproveUser(long approvedByuserId, string defaultPass, string userEmail) ===>{ex.Message}");
                throw;
            }
        }

        public async Task<dynamic> DeclineUser(long disapprovedByuserId, string userEmail, string comment)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.APPROVEUSER);
                    param.Add("@UserIdDisapprove", disapprovedByuserId);
                    param.Add("@UserEmailDisapprove", userEmail);
                    param.Add("@DisapprovedComment", comment);

                    dynamic resp = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return resp;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: DeclineUser(long disapprovedByuserId, string userEmail, string comment) ===>{ex.Message}");
                throw;
            }
        }

        public async Task<dynamic> DeactivateUser(long deactivatedByuserId, string userEmail, string comment)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.DEACTIVATEUSER);
                    param.Add("@UserIdDeactivate", deactivatedByuserId);
                    param.Add("@UserEmailDeactivate", userEmail);
                    param.Add("@DeactivatedComment", comment);

                    dynamic resp = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    Console.WriteLine("DEACTIVATION RESPONSE -- " + resp);

                    return resp;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: DeactivateUser(long deactivatedByuserId, string userEmail, string comment) ===>{ex.Message}");
                throw;
            }
        }

        public async Task<dynamic> ReactivateUser(long reactivatedByuserId, string userEmail, string comment)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.REACTIVATEUSER);
                    param.Add("@UserIdDeactivate", reactivatedByuserId);
                    param.Add("@UserEmailDeactivate", userEmail);
                    param.Add("@DeactivatedComment", comment);

                    dynamic resp = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    Console.WriteLine("Re-activated comment -- " + resp);

                    return resp;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: ReactivateUser(long reactivatedByuserId, string userEmail, string comment) ===>{ex.Message}");
                throw;
            }
        }

        public async Task<dynamic> UnblockUser(long unblockedByuserId, string defaultPassword, string userEmail)
        {
            try
            {
                var hashdefaultPassword = BCrypt.Net.BCrypt.HashPassword(defaultPassword, BCrypt.Net.BCrypt.GenerateSalt());

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.UNBLOCKUSER);
                    param.Add("@UnblockDefaultPassword", hashdefaultPassword);
                    param.Add("@UnblockedByUserId", unblockedByuserId);
                    param.Add("@UserEmailUnblock", userEmail);

                    dynamic resp = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return resp;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: UnblockUser(long unblocedByuserId, string userEmail) ===>{ex.Message}");
                throw;
            }
        }


        public async Task<dynamic> ChangePassword(long userId, string newPassword)
        {
            try
            {
                var hashNewPassword = BCrypt.Net.BCrypt.HashPassword(newPassword, BCrypt.Net.BCrypt.GenerateSalt());

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@Status", Account.CHANGEPASSWORD);
                    param.Add("@PasswordHashModifr", hashNewPassword);
                    param.Add("@UserIdModifr", userId);

                    dynamic resp = await _dapper.ExecuteAsync(ApplicationConstant.Sp_UserAuthandLogin, param: param, commandType: CommandType.StoredProcedure);

                    return resp;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: ChangePassword(long userId, string newPassword) ===>{ex.Message}");
                throw;
            }
        }

        public void SendEmail(string recipientEmail, string firtname, string defaultPass, string subject, string wwwRootPath, string ip, string port, string appKey = null, string channel = null)
        {
            string emailAddress = _configuration["Smtp:EmailAddress"];
            string smtpAdress = _configuration["Smtp:Host"];
            int smtpPort = Convert.ToInt16(_configuration["Smtp:Port"]);
            string password = _configuration["Smtp:Password"];
            //string sender = _configuration["Smtp:Sender"];

            //var sampleEmail = "yusufsunkanmi3@gmail.com";

            string message = string.Empty;
            MimeMessage mailBody = new MimeMessage();

            MailboxAddress from = new MailboxAddress("Xpress Monitor", emailAddress);
            mailBody.From.Add(from);

            //MailboxAddress to = new MailboxAddress("User", recipientEmail);
            MailboxAddress to = new MailboxAddress("User", recipientEmail);
            mailBody.To.Add(to);

            mailBody.Subject = subject;

            if (subject.ToLower().Contains("unblock"))
            {
                message = ComposeEmailToUnblockAccount(firtname, defaultPass, recipientEmail, ip, port);
            }
            else if (subject.ToLower().Contains("password"))
            {
                message = ComposeEmailForPasswordChange(firtname, defaultPass, recipientEmail, ip, port);
            }
            else
            {
                message = ComposeSignUpMail(firtname, defaultPass, recipientEmail, wwwRootPath, ip, port, appKey, channel);
            }

            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = message;
            mailBody.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
            client.Connect(smtpAdress, smtpPort, false);
            client.Authenticate(emailAddress, password);
            client.Send(mailBody);
            client.Disconnect(true);
            client.Dispose();
        }
        public string ComposeSignUpMail(string firstname, string defaultPass, string email, string wwwRootPath, string ip, string port, string appKey = null, string channel = null)
        {

            string message = string.Empty;
            string body = string.Empty;
            string templatePath = string.Empty;

            if (null == channel)
            {
                string qryStr = string.Empty;
                string clientUrl = _configuration["FrontEnd:FrontEndUrl"];
                //string clientUrl = $"http://{ip}:{port}/";
                templatePath = $"{wwwRootPath}/EmailHandler/SignUp.html";
                if (appKey == null)
                {
                    qryStr = $"?k={defaultPass}&a={email}";
                }
                else
                {
                    qryStr = $"?k={defaultPass}&a={email}&appkey={appKey}";
                }
                message = $"Dear {firstname}," +
                          $"<p>Thanks for registering on our platform. Please click on the link below to activate your account.</p>";

                using (StreamReader reader = new StreamReader(Path.Combine(templatePath)))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{link}", $"{clientUrl}{qryStr}");
                body = body.Replace("{MailContent}", message);

                return body;
            }
            else
            {
                templatePath = $"{wwwRootPath}/EmailHandler/SignUpWithToken.html";
                message = $"Dear {firstname}," +
                          $"<p>Thanks for registering on our platform. Use the following OTP to complete your Sign Up procedures.</p>";

                using (StreamReader reader = new StreamReader(Path.Combine(templatePath)))
                {
                    body = reader.ReadToEnd();
                }

                body = body.Replace("{OTP}", defaultPass);
                body = body.Replace("{MailContent}", message);

                return body;
            }
        }

        public string ComposeEmailForPasswordChange(string firstname, string defaultPass, string email, string ip, string port)
        {
            string clientUrl = $"http://{ip}:{port}/";
            //string clientUrl = _configuration["SSO:FrontEndUrl"];
            string qryStr = $"?k={defaultPass}&a={email}";

            string message = $"Hello {firstname}," +
                          $"{Environment.NewLine} {Environment.NewLine}You are receiving this email because we recieved a password reset request for your account." +
                          $"{Environment.NewLine} {Environment.NewLine}Please click on the link below to reset your account." +
                          $"{Environment.NewLine} {Environment.NewLine}{clientUrl}{qryStr}" +
                          $"{Environment.NewLine} {Environment.NewLine}Warm Regards," +
                          $"{Environment.NewLine} {Environment.NewLine}Xpress Account";

            return message;
        }

        public string ComposeEmailToUnblockAccount(string firstname, string defaultPass, string email, string ip, string port)
        {
            //string clientUrl = $"http://{ip}:{port}/";
            string clientUrl = _configuration["SSO:FrontEndUrl"];
            string qryStr = $"?k={defaultPass}&a={email}";

            string message = $"Hello {firstname}," +
                          $"{Environment.NewLine} {Environment.NewLine}You are receiving this email because we recieved a password reset request for your account." +
                          $"{Environment.NewLine} {Environment.NewLine}Please click on the link below to reset your account." +
                          $"{Environment.NewLine} {Environment.NewLine}{clientUrl}{qryStr}" +
                          $"{Environment.NewLine} {Environment.NewLine}Warm Regards," +
                          $"{Environment.NewLine} {Environment.NewLine}Xpress Account";

            return message;
        }

    }
}
