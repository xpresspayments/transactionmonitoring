﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PosMonitoring.Data.UserAccount.IRepository;
using PosMonitoring.GenericResponse;
using PosMonitoring.ViewModels;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PosMonitoring.Data.UserAccount.Repository
{
    public class TokenRefresher : ITokenRefresher
    {
        private readonly IJwtManager _jwtManager;
        private readonly IConfiguration _configuration;
        private readonly IAccountRepository _accountRepository;

        public TokenRefresher(IConfiguration configuration, IJwtManager jwtManager, IAccountRepository accountRepository)
        {
            _jwtManager = jwtManager;
            _configuration = configuration;
            _accountRepository = accountRepository;
        }

        public async Task<AuthResponse> Refresh(RefreshTokenModel request)
        {
            string _key = _configuration["Jwt:Key"];
            var principal = new ClaimsPrincipal();
            SecurityToken validatedToken;
            JwtSecurityToken jwtToken = new JwtSecurityToken();
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                principal = tokenHandler.ValidateToken(Encoding.UTF8.GetString(Convert.FromBase64String(request.JwtToken)), new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_key))
                }, out validatedToken);

                jwtToken = validatedToken as JwtSecurityToken;
            }
            catch (Exception ex)
            {
                if(ex.Message.Contains("The token is expired"))
                {
                    var rdetails = await _accountRepository.FindUser(request.Email);

                    if (request.RefreshToken != rdetails.RefreshToken)
                    {
                        return new AuthResponse { JwtToken = "", RefreshToken = "", Message = "Invalid Token or Refresh Token was detected" };
                    }

                    return await _jwtManager.RefreshJsonWebToken(rdetails.UserId, principal.Claims.ToArray());
                }
            }        

            

            if (jwtToken == null || jwtToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256Signature))
            {
                //throw new SecurityTokenException("Invalid token passed");
                return new AuthResponse { JwtToken = "", RefreshToken = "", Message = "Invalid Token passed" };
            }

            var userEmailFromClaim = principal.Identity.Name;
            if(string.IsNullOrWhiteSpace(userEmailFromClaim))
                //throw new SecurityTokenException("Invalid token passed");
                return new AuthResponse { JwtToken = "", RefreshToken = "", Message = "Invalid Token passed" };

            var details = await _accountRepository.FindUser(userEmailFromClaim);

            if (details == null)
                return new AuthResponse { JwtToken = "", RefreshToken = "", Message = "Invalid Token passed" };

            if (request.RefreshToken != details.RefreshToken)
            {
                return new AuthResponse { JwtToken = "", RefreshToken = "", Message = "Invalid Token or Refresh Token was detected" };
            }

            return await _jwtManager.RefreshJsonWebToken(details.UserId, principal.Claims.ToArray());
        }


    }
}
