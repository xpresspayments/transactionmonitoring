﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Data.UserAccount.IRepository
{
    public interface IRefreshTokenGenerator
    {
        string GenerateRefreshToken();
    }
}
