﻿using PosMonitoring.Dtos.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosMonitoring.Data.UserAccount.IRepository
{
    public interface IAccountRepository
    {
        Task<User> FindUser(string email);
        Task<CreateUserResponse> AddUser(CreateUserDto user, int createdbyUserId);
        Task<dynamic> UpdateUser(UpdateUserDto user, int updatedbyUserId);
        Task<IEnumerable<User>> GetAllUsers();
        Task<IEnumerable<User>> GetUsersPendingApproval();
        Task<dynamic> ApproveUser(long approvedByuserId, string defaultPass, string userEmail);
        Task<dynamic> DeclineUser(long disapprovedByuserId, string userEmail, string comment);
        Task<dynamic> DeactivateUser(long deactivatedByuserId, string userEmail, string comment);
        Task<dynamic> ReactivateUser(long reactivatedByuserId, string userEmail, string comment);
        Task<dynamic> UnblockUser(long unblocedByuserId, string defaultPassword, string userEmail);
        Task<dynamic> ChangePassword(long userId, string newPassword);
        void SendEmail(string recipientEmail, string firtname, string defaultPass, string subject, string wwwRootPath, string ip, string port, string appKey = null, string channel = null);
    }
}
