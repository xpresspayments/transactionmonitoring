using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PosMonitoring.AppConstants;
using PosMonitoring.Dtos;
using PosMonitoring.Enums;
using PosMonitoring.Models;
using PosMonitoring.Wrappers;

namespace PosMonitoring.Data
{
    public class TransactionRepo : ITransactionRepo
    {
        private readonly string _connectionString;
        private readonly ILogger<TransactionRepo> _logger;
        private readonly IBankPrefixRepo _prefix;
        private readonly IUriService _uriService;
        private readonly IMapper _mapper;

        public TransactionRepo(IConfiguration configuration, IUriService uriService, IMapper mapper, ILogger<TransactionRepo> logger, IBankPrefixRepo prefix)
        {
            _connectionString = configuration.GetConnectionString("DefaultConnection");
            _uriService = uriService;
            _mapper = mapper;
            _logger = logger;
            _prefix = prefix;
        }


        //public void CreatePosTransaction(Notification txn)
        //{
        //    //throw new System.NotImplementedException();
        //    if (null == txn)
        //    {
        //        throw new ArgumentNullException(nameof(txn));
        //    }

        //    _context.PosTransactions.Add(txn);
        //}

        //public IEnumerable<Notification> GetAllTransactions()
        //{
        //    //throw new System.NotImplementedException();
        //    return _context.PosTransactions.ToList();
        //}


        //FILTERED TRANSACTIONS RESULT SHOULD ALSO CHANGE THE SUMMARY AT THE TOP BASED ON THE RESULT
        public async Task<PagedResponse<List<Notification>>> PosTransactionsFilter(TransactionFilterDto filterRequest, PaginationFilter validFilter, string route, string userBankId, int groupId, string accType)
        {
            try
            {
                var filteredReport = new TransactionReadDtoVm();
                var request = InputFormatter(filterRequest);
                var requestedBankId = (Convert.ToInt32(userBankId) > 0) ? Convert.ToInt32(userBankId) : 0;

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var filterParam = new DynamicParameters();
                    filterParam.Add("@Status", Status.FILTER);
                    filterParam.Add("@MerchantId", request.MerchantId);
                    filterParam.Add("@TerminalId", request.TerminalId);
                    filterParam.Add("@TransactionStatus", request.TransactionStatus);
                    filterParam.Add("@CustomerName", request.CustomerName);
                    filterParam.Add("@RRN", request.RRN);
                    filterParam.Add("@FromDate", request.FromDate);
                    filterParam.Add("@ToDate", request.ToDate);
                    filterParam.Add("@GetBankIdFilter", requestedBankId);

                    //if (accType == "MERCHANT_USER")
                    //{
                        filterParam.Add("@UserAccountType", accType);
                        filterParam.Add("@UserGroupId", groupId);
                    //}

                    var rspParam = await _dapper.QueryAsync<Notification>(ApplicationConstant.Sp_PosTransactionReport, param: filterParam, commandType: CommandType.StoredProcedure);

                    var pagedData = rspParam
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                    var totalRecords = rspParam.Count();

                    var pagedReponse = PaginationHelper.CreatePagedReponse<Notification>(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<Notification>> response = pagedReponse;
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: TransactionFilter() ===>" + ex.Message.ToString());
                throw;
            }
        }

        public async Task<PagedResponse<List<Notification>>> GetPaginatedTransactions(PaginationFilter validFilter, string route, string bankId, int groupId, string accType)
        {
            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
               
                    var fetchParam = new DynamicParameters();
                    fetchParam.Add("@Status", Status.PAGINETED);
                    fetchParam.Add("@UserBankId", bankId);
                    //if (accType == "MERCHANT_USER")
                    //{
                        fetchParam.Add("@UserAccountType", accType);
                        fetchParam.Add("@UserGroupId", groupId);
                    //}

                    var rResponse = await _dapper.QueryAsync<Notification>(ApplicationConstant.Sp_PosTransactionReport, param: fetchParam, commandType: CommandType.StoredProcedure);

                    var pagedData = rResponse
                                       .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                       .Take(validFilter.PageSize).ToList();

                    var totalRecords = rResponse.Count();

                    var pagedReponse = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<Notification>> response = pagedReponse;
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetPaginatedTransactions(PaginationFilter validFilter, string route) ===>" + ex.Message.ToString());
                throw;
            }

        }

        //THIS METHOD SHOULD BE MODIFIED TO ADD DATE
        //DEFAULT DASHBOARD TRANSACTION SUMMARY SHOULD BE FOR CURRENT DAY
        public async Task<DashboardDetailsDto> GetTransactionsAppDashboardTransactions(string bankId, int groupId, string accType)
        {
            var requestedBankId = (Convert.ToInt32(bankId) > 0) ? Convert.ToInt32(bankId) : 0;

            try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var gParam = new DynamicParameters();
                    gParam.Add("@Status", Status.DASHBOARD);
                    gParam.Add("@RequesterBankId", requestedBankId);
                    if (accType == "MERCHANT_USER")
                    {
                        gParam.Add("@UserAccountType", accType);
                        gParam.Add("@UserGroupId", groupId);
                    }

                    //Console.WriteLine("--- Bank ID Received in repository ---" + requestedBankId);

                    var rResponse = await _dapper.QueryFirstOrDefaultAsync<DashboardDetailsDto>(ApplicationConstant.Sp_DashboardTransactionReport, param: gParam, commandType: CommandType.StoredProcedure);

                    return rResponse;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetTransactionsAppDashboardTransactions() ===>" + ex.Message.ToString());
                throw;
            }
        }

        public async Task<PagedResponse<List<TransactionReadDto>>> GetApprovedTransactions(PaginationFilter validFilter, string route, string bankId, int groupId, string accType)
        {
            try
            {
                var requestedBankId = (Convert.ToInt32(bankId) > 0) ? Convert.ToInt32(bankId) : 0;

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var aParam = new DynamicParameters();
                    aParam.Add("@Status", Status.APPROVED);
                    aParam.Add("@BankIdRequester", requestedBankId);
                    if (accType == "MERCHANT_USER")
                    {
                        aParam.Add("@UserAccountType", accType);
                        aParam.Add("@UserGroupId", groupId);
                    }

                    var aResponse = await _dapper.QueryAsync<TransactionReadDto>(ApplicationConstant.Sp_BankDeclinedAndApproved, param: aParam, commandType: CommandType.StoredProcedure);


                    var pagedData = aResponse
                                   .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                   .Take(validFilter.PageSize).ToList();

                    var totalRecords = aResponse.Count();

                    var pagedReponse = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<TransactionReadDto>> response = pagedReponse;
                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetApprovedTransactions() ===>" + ex.Message.ToString());
                throw;
            }
        }

        //GET SINGLE TRANSACTION FOR VIEW DETAILS AND E-JOURNAL
        public async Task<TransactionReadDto> GetTransactionById(int id)
        {
             try
            {
                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var closeParam = new DynamicParameters();
                    closeParam.Add("@id", id);
                    string resultQuery = "SELECT * FROM Notification WHERE id = @id";
                    var response = await _dapper.QuerySingleOrDefaultAsync<TransactionReadDto>(resultQuery, param: closeParam);

                     return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetTransactionById() ===>" + ex.Message.ToString());
                throw;
            }
        }

        public async Task<PagedResponse<List<TransactionReadDto>>> GetDeclinedTransactions(PaginationFilter validFilter, string route, string bankId, int groupId, string accType)
        {
            try
            {
                var requestedBankId = (Convert.ToInt32(bankId) > 0) ? Convert.ToInt32(bankId) : 0;

                using (SqlConnection _dapper = new SqlConnection(_connectionString))
                {
                    var dParam = new DynamicParameters();
                    dParam.Add("@Status", Status.DECLINED);
                    dParam.Add("@BankIdRequester", requestedBankId);
                    if (accType == "MERCHANT_USER")
                    {
                        dParam.Add("@UserAccountType", accType);
                        dParam.Add("@UserGroupId", groupId);
                    }
                        
                    var dResponse = await _dapper.QueryAsync<TransactionReadDto>(ApplicationConstant.Sp_BankDeclinedAndApproved, param: dParam, commandType: CommandType.StoredProcedure);

                    

                    var pagedData = dResponse
                                   .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
                                   .Take(validFilter.PageSize).ToList();

                    var totalRecords = dResponse.Count();

                    

                    var pagedReponse = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _uriService, route);

                    PagedResponse<List<TransactionReadDto>> response = pagedReponse;

                    //Console.WriteLine(response);

                    return response;
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
                _logger.LogError($"MethodName: GetDeclinedTransactions() ===>" + ex.Message.ToString());
                return null;
            }
        }

        private TransactionFilterFormatterDto InputFormatter(TransactionFilterDto request)
        {
            TransactionFilterFormatterDto FilterRequest;
            try
            {
                FilterRequest = (new TransactionFilterFormatterDto()
                {
                    TransactionId = request.TransactionId == null ? "" : request.TransactionId.ToString(),
                    MerchantId = request.MerchantId == null ? "" : request.MerchantId.ToString(),
                    TerminalId = request.TerminalId == null ? "" : request.TerminalId.ToString(),
                    TransactionStatus = request.TransactionStatus == null ? "" : request.TransactionStatus.ToString(),
                    CustomerName = request.CustomerName == null ? "" : request.CustomerName.ToString(),
                    RRN = request.RRN == null ? "" : request.RRN.ToString(),
                    FromDate = request.FromDate == null ? "" : request.FromDate.ToString(),
                    ToDate = request.ToDate == null ? "" : request.ToDate.ToString()
                });

                return FilterRequest;
            }
            catch (Exception ex)
            {
                var Error = ex.Message;
                _logger.LogError($"MethodName: InputFormatter(TransactionFilterDto request) ===>" + ex.Message.ToString());
                throw;
            }
        }
        
        
    }
}