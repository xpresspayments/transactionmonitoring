﻿namespace PosMonitoring.Enums
{
    public enum Status
    {
        INSERT = 1,
        UPDATE = 2,
        DELETE = 3,
        GETALL = 4,
        GETBYID = 5,
        FILTER = 6,
        PAGINETED = 7,
        DASHBOARD = 8,
        APPROVED = 9,
        DECLINED = 10
    }
}
