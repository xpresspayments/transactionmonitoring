﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PosMonitoring.ViewModels
{
    public class UserId
    {
        public long Id { get; set; }
        public bool IsRemoved { get; set; }
    }
}
