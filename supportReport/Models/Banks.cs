﻿using System;
using System.ComponentModel.DataAnnotations;
namespace PosMonitoring.Models
{
    public class Banks
    {
        [Key]
        public int Id { get; set; }
        public string BankName { get; set; }
        public string TerminalIdPrefix { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }
}
