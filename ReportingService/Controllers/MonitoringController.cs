﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace ReportingService.Controllers
{
    [Route("api/c/[controller]")]
    [ApiController ]
    public class MonitoringController : ControllerBase
    {
        public MonitoringController()
        {

        }

        [HttpPost]
        public ActionResult TestInboundConnection()
        {
            Console.WriteLine("--- Inbound POST # Reporting service ---");

            return Ok("Inbound test Ok");
        }
    }
}
