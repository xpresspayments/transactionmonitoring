# CODE DOCUMENTATION #

This explains our to run the application (POS Monitoring app) for both `frontend` and `backend` appas.

### How to start the backend app, after you clone the repository to your local machine? ###

* `cd PosMonitoring` either directly from your terminal or within your IDE/Text Editor (e.g Visual Studio)
* `dotnet run` (you must have .NET 5+ CLI install) or run the code directly from your IDE/Code Editor
* Open `appsetting.json`, Set your DB port, username and password to the corresponding details of your database instance
* Your app is now accessible on `http://localhost:5000` and an instance of swagger at `https://localhost:5001` for API documentation 

### How to start the frontend application (VueJs) ###

* `cd New_Frontend/xpress` after cloning the repository
* `npm install` - You must have `Node.js` and `npm` installed on your machine 
* `npm run dev` to start - The frontend application would start on `localhost:3000`
* Open `main.js` and replace the value of `app.config.globalProperties.$apiBaseUrl` with the appropriate backend API endpoint  `https://monitorapi.xpresspayments.com` is for production, `https://monitortestapi.xpresspayments.com` is for test environment, Or `{LOCALHOST:PORT` corresponding to your `localhost` and `port` if the backend is running locally.
* Do the same to replace the value of `API_URL` in `src/services/auth.service.js` on line 3
* Login to the application using the default super admin login details : 
	Username: `john.doe@xpresspayments.com`, Password: `password123` - For production only
	Username: `superadmin@xpresspayments.com`, Password: `password123` - For test environment 


### How to deploy the backend? ###

* On your terminal run `docker login` - You must have Docker desktop install, and must have access to XPress Payment Dockerhub account
* Make sure the database connection in the `appsetting.json` file is changed to production DB
* `cd PosMonitoring` - You must be on the same directory level with `Dockerfile`
* `docker build -t {YOUR_DOCKERHUB_USERNAME}/posmonitoring .`
* `docker push {YOUR_DOCKERHUB_USERNAME}/posmonitoring`
* pull the changes on the server - this is internally handled by XPress Payments' system admin

### How to deploy the frontend? ###

* `cd New_Frontend/xpress`
* `npm run build` - Make sure the API endpoints on the frontend files are changed to the production endpoints
* The build would be saved into `dist` folder
* Zip and share the `dist` folder with the XPress Payments' system admin team for deployment.
